"""


Usage:

Output:

Authored by Hongsuk Kang
Created: 07-28-2021

Copyright (c) 2021 Quantum Intelligence Corporation
"""
import sys
import argparse
import numpy as np


def dense_to_sparse(dense_coords, dh):
    """Convert dense representation to sparse representation.

    Ex) dense = [[1.0, 0.0, -1.0], [-2.0, 2.3, 3.3], [-3.4, 2.2, 5.1], [-4.0, -1.1, 2.3]]
    print (dense_to_sparse(dense))

    [[[0. 0. 0. 1. 0. 0. 0.]
      [0. 0. 0. 0. 0. 0. 0.]
      [0. 0. 0. 0. 0. 0. 0.]
      [0. 0. 0. 0. 0. 0. 1.]]

    [[0. 0. 0. 0. 0. 0. 0.]
     [0. 0. 0. 0. 0. 0. 0.]
     [0. 0. 0. 0. 0. 0. 0.]
     [0. 0. 0. 0. 0. 0. 0.]]

    [[0. 0. 0. 0. 0. 0. 0.]
     [0. 0. 0. 0. 0. 0. 0.]
     [0. 0. 0. 0. 0. 0. 0.]
     [0. 0. 0. 0. 1. 0. 0.]]

    [[0. 0. 0. 0. 0. 0. 0.]
     [0. 0. 0. 0. 0. 0. 0.]
     [0. 0. 0. 0. 0. 0. 0.]
     [0. 0. 0. 0. 0. 0. 0.]]

    [[0. 0. 0. 0. 0. 0. 0.]
     [1. 0. 0. 0. 0. 0. 0.]
     [0. 0. 0. 0. 0. 0. 0.]
     [0. 0. 0. 0. 0. 0. 0.]]]

    Args:
        dense_coords (ndarray-like (3D)): Dense representation of coordinates.
        dh (float): Mesh size.

    Returns:
        ndarray (3D) : Sparse representation
    """
    centroid = np.average(dense_coords, axis=0)
    lower_bound = np.min(dense_coords, axis=0) + \
        np.array([dh/2.0, dh/2.0, dh/2.0])
    upper_bound = np.max(dense_coords, axis=0) + \
        np.array([dh/2.0, dh/2.0, dh/2.0])

    nmeshes = np.ceil((upper_bound - lower_bound) / dh)
    # gap = nmeshes * dh - (upper_bound - lower_bound)

    # lower_bound -= gap/2
    # upper_bound += gap/2

    nmeshes = np.array(nmeshes, dtype=int)

    sparse_coords = np.ndarray(shape=nmeshes)
    sparse_coords.fill(0.0)

    for dense_coord in dense_coords:
        sparse_coord = np.array(np.rint((dense_coord - lower_bound) / dh), dtype=int) - \
            np.array([1, 1, 1], dtype=int)
        #print(sparse_coord[0], sparse_coord[1], sparse_coord[2])
        sparse_coords[sparse_coord[0]][sparse_coord[1]][sparse_coord[2]] = 1.0

    return sparse_coords

if __name__ == "__main__":
    # Example
    A = [[1.0, 0.0, -1.0], [-2.0, 2.3, 3.3], [-3.4, 2.2, 5.1], [-4.0, -1.1, 2.3]]
    print (dense_to_sparse(A, 1.0))