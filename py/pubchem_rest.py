"""Searches molecules with SMILES and fetches it from Pubchem site


Usage: python3 -m pubchem_rest --smiles smiles --cids cids --overwrite

Output:

Authored by Hongsuk Kang
Modified: 03-16-2021
Created: 01-10-2021

Copyright (c) 2021 Quantum Intelligence Corporation
"""
import time
import sys
import argparse
import requests
import pathlib
import urllib


def save_sdf(outpath, sdftext, overwrite=False):
    """Saves a SDF text to outpath if not exists or overwrite is True.

    Args:
        outpath (pathlib.Path): A output file path
        sdftext (str): A SDF file content.
        overwrite (bool, optional): Overwrites if True. Defaults to False.
    """
    if overwrite or (not overwrite and not outpath.exists()):
        with open(outpath, 'w') as fw:
            fw.write(sdftext)


def fetch_pubchemid_with_smiles(smiles, with_sids = False):
    """Fetches the Pubchem CID of a compound having a given SMILES code.

    Args:
        smiles (str): An input SMILES code.

    Raises:
        ValueError: The Pubchem CID is not found.

    Returns:
        A list of str: The Pubchem CIDs.
        A dict of str: The SIDs for each CID compound.
    """
    # response = requests.post(
    #     "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/smiles/conformers/json", data={"smiles": smiles, "cids_type": "same_connectivity"})

    smiles_encoded = urllib.parse.quote_plus("smiles")

    #print (smiles_encoded)

    response = requests.get(
        "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/fastidentity/smiles/%s/json?identity_type=same_connectivity" % (smiles) )

    if response.status_code != 200:
        raise ValueError('POST /tasks/ {}'.format(response.status_code))

    compounds_information = response.json()

    cids = []
    sids = {}
    
    for item in compounds_information["PC_Compounds"]:
        #print(item)
        cid = item["id"]["id"]["cid"]

        cids.append(cid)

        similar_response = requests.get(
            "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/%s/cids/json?cids_type=same_connectivity" % (cid))

        similar_json = similar_response.json()
        #print(similar_json)
        for similar_cid in similar_json["IdentifierList"]["CID"]:
            cids.append(similar_cid)

    cids = list(map(str, set(cids)))

    if with_sids:
        sid_response = requests.post(
            "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/conformers/json", data={'cid': ",".join(cids)})

        if sid_response.status_code == 200:
            sid_json = sid_response.json()

            for item in sid_json["InformationList"]["Information"]:
                sids[item["CID"]] = []
                sids[item["CID"]].extend(item["ConformerID"])

    return cids, sids


def fetch_sdf(cid):
    """Fetches SDF text with a Pubchem CID from Pubchem site

    Arguments:
        smiles (str): A SMILES code for a target compound

    Returns:
        str: SDF text
    """
    response = requests.get(
        "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/%s/SDF?record_type=3d" % (cid))

    if response.status_code != 200:
        raise ValueError('GET /tasks/ {}'.format(response.status_code))

    return response.text


def get_parent_cids(cids):
    """Gets the Pubchem CID of the parent compound for a compound with cid.

    Args:
        cid (str): A Pubchem CID.

    Returns:
        dict: The CID of the parent compound.
    """
    parent_cids = {}
    for cid in cids:
        response = requests.get(
            "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/%s/cids/JSON?cids_type=parent" % (cid))

        if response.status_code == 200:
            parent_cids_json = response.json()

            parent_cid = str(parent_cids_json["IdentifierList"]["CID"][0])

            parent_cids[cid] = parent_cid if cid != parent_cid else ''
        else:
            parent_cids[cid] = ''

    return parent_cids


def get_component_cids(cids):
    """Gets the Pubchem CIDs of the component compound in CID.

    Args:
        cid (str): A Pubchem CID.

    Returns:
        list: The CIDs of the components. A list of strs.
    """
    component_cids = {}
    for cid in cids:
        response = requests.get(
            "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/%s/cids/JSON?cids_type=component" % (cid))

        if response.status_code == 200:
            component_cids_json = response.json()

            component_cids[cid] = list(
                map(str, component_cids_json["IdentifierList"]["CID"]))
        else:
            component_cids[cid] = []

    return component_cids


def save_sdfs_with_smiles(smiles_filepath, overwrite=False):
    """Searches Pubchem CIDs with SMILES codes, fetches and saves the SDF
    files corresponding to the CIDs.

    If the SDF files are not available for SMILES, then it returns the list of
    the uncompleted SMILES codes.

    The acceptable format of SMILES files is at least two columns separated
    by white spaces. The first is a SMILES code and the second is a molecule's
    name.

    ex)
    CCC Mol1
    C(ON=)C Mol2

    Args:
        smiles_filepath (str): A SMILES code file path.
        overwrite (bool): Overwrites if True. Defaults to False.

    Returns:
        list: A list of the uncompleted SMILES codes.
    """
    uncompleted_smiles = []
    with open(smiles_filepath, 'r') as f:
        for line in f:
            tokens = line.split()

            assert(len(tokens) == 2)

            smiles, molname = tokens

            try:
                outpath = pathlib.Path(molname + ".sdf")

                cids, sids = fetch_pubchemid_with_smiles(smiles)
                cid = cids[0]

                if overwrite or (not overwrite and not outpath.exists()):
                    sdf = fetch_sdf(cid)
                    save_sdf(outpath, sdf, overwrite)

            except ValueError as e:
                uncompleted_smiles.append(smiles)

    return uncompleted_smiles


def save_sdfs_with_cids(cid_filepath, overwrite=False):
    """Fetches and saves the SDF files with the CIDs listed in cid filepath.
    If the SDF files are not available for CIDs, then it returns the list of
    the uncompleted CIDs.

    Args:
        cid_filepath (str): A file path for the CID list.
        overwrite (bool): Overwrites if True. Defaults to False.

    Returns:
        list: A list of the uncompleted CIDs.
    """
    uncompleted_cids = []
    with open(cid_filepath) as f:
        for line in f:
            cids = line.strip().split(",")

            for cid in cids:
                if cid != "0":
                    try:
                        outpath = pathlib.Path("%s.sdf" % (cid))

                        if overwrite or (not overwrite and not outpath.exists()):
                            sdf = fetch_sdf(cid)
                            save_sdf(outpath, sdf, overwrite)

                    except (ValueError, TimeoutError) as e:
                        uncompleted_cids.append(cid)

    return uncompleted_cids


def save_parent_sdfs(cids, overwrite=False):
    """Fetches a SDF text of the parent compound of each CID ans saves
     it with the name of the CID.

    Args:
        cids (list): A list of CIDs.

    Returns:
        list: The list of the uncompleted compound CIDs. A list of strs.
    """
    uncompleted_cids = []
    for cid in cids:
        try:
            parent_cid = parent_cids[cid]

            if parent_cid:
                outpath = pathlib.Path(cid + ".sdf")
                sdf = fetch_sdf(parent_cid)

                save_sdf(outpath, sdf, overwrite)
        except ValueError:
            uncompleted_cids.append(cid)

    return uncompleted_cids


def save_component_sdfs(cids, component_cid_dict, overwrite=False):
    """Fetches a SDF text of the component compound of each CID ans saves
     it with the name of the CID.

    Args:
        cids (list): A list of CIDs.
        component_cid_dict (dict): A dictionary of CID to component CIDs.

    Returns:
        list: The list of the uncompleted compound CIDs. A list of strs.
    """
    uncompleted_cids = []
    for cid in cids:
        try:
            component_cids = component_cid_dict[cid]

            if component_cids:
                outpath = pathlib.Path(cid + ".sdf")

                with open(outpath, "w") as fw:
                    for component_cid in component_cids:
                        sdf = fetch_sdf(component_cid)
                        fw.write(sdf)

        except ValueError:
            uncompleted_cids.append(cid)

    return uncompleted_cids


def fetch_smiles_with_pubchem_id(cid):
    """Fetches SDF text with a Pubchem CID from Pubchem site

    Arguments:
        smiles (str): A SMILES code for a target compound

    Returns:
        str: SDF text
    """
    response = requests.get(
        "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/%s/JSON" % (cid))

    if response.status_code != 200:
        raise ValueError('GET /tasks/ {}'.format(response.status_code))

    information = response.json()

    for item in information["PC_Compounds"][0]["props"]:
        if item["urn"]["label"] == "SMILES":
            return item["value"]["sval"]


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--smiles", "-s", type=str)
    parser.add_argument("--cids", "-c", type=str)
    parser.add_argument("--overwrite", "-v",
                        action='store_true', default=False)

    args = parser.parse_args()

    if args.smiles:
        uncompleted_smiles = save_sdfs_with_smiles(args.smiles, args.overwrite)

    if args.cids:
        uncompleted_cids = save_sdfs_with_cids(args.cids, args.overwrite)

        parent_cids = get_parent_cids(uncompleted_cids)
        uncompleted_cids = save_parent_sdfs(parent_cids)

        component_cids = get_component_cids(uncompleted_cids)
        uncompleted_cids = save_component_sdfs(
            uncompleted_cids, component_cids)

        for uncompleted_cid in uncompleted_cids:
            print(uncompleted_cid)
