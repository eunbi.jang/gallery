from quest_io import read_mol2str, write_mol2file, write_mol2str
import argparse
import re
from io import StringIO

def convert_hydrogen_atomtypes(aname_neighbor, aname_target):
    convert_table = {'N': 'H.2', 'O': 'H.3', 'S': 'H.4'}
    aname_neighbor = re.sub(r'[0-9]*', '', aname_neighbor)
    if aname_neighbor in convert_table.keys():
        return convert_table[aname_neighbor]
    else:
        return aname_target

def convert_solvation_atomtypes(mol2str):
    mols, molnames = read_mol2str(mol2str)
    for mol in mols:
        for ai, atom in enumerate(mol):

            # Convert hydrogens
            if 'H' in atom.aname:
                for aj in atom.neighbors:
                    atomj_aname = mol[aj].aname
                    atom.atom_type = convert_hydrogen_atomtypes(
                        atomj_aname, atom.atom_type)

                for aj in range(len(mol)):
                    if ai in mol[aj].neighbors:
                        atomj_aname = mol[aj].aname
                        atom.atom_type = convert_hydrogen_atomtypes(
                            atomj_aname, atom.atom_type)

            # Convert oxygens
            if 'O' in atom.aname:
                # How many neighbor carbons?
                carbons = 0
                oxygens = 0
                for aj in atom.neighbors:
                    atomj_aname = mol[aj].aname
                    if 'C' in atomj_aname:
                        carbons += 1
                    if 'O' in atomj_aname:
                        oxygens += 1

                if carbons == 2 and oxygens == 0:
                    atom.atom_type = "O.4"
                elif carbons == 1 and oxygens == 1:
                    atom.atom_type = "O.5"

    str = StringIO()
    write_mol2str(str, mols, molnames)

    return str.getvalue()
    # write_mol2file(args.output, mols, molnames)
