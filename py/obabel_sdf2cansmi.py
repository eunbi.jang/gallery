"""Sample code for openbabel conversions

Usage:

Output:

Authored by Hongsuk Kang
Created: 04-06-2021

Copyright (c) 2021 Quantum Intelligence Corporation
"""

import sys
import argparse

import openbabel as ob

def convert_sdf2cansmi(sdf):
    """Return mol2 text with an input SMILES

    Args:
        smi (str): An input SMILES code.

    Returns:
        str: A mol2 text generated with the input SMILES.
    """
    obConversion = ob.OBConversion()
    obConversion.SetInAndOutFormats("sdf", "can")

    mol = ob.OBMol()
    obConversion.ReadString(mol, sdf)

    return obConversion.WriteString(mol)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--sdf", "-s", type=str, required=True)

    args = parser.parse_args()

    with open(args.sdf) as f:
        sdf = f.read()
        print (convert_sdf2cansmi(sdf))
