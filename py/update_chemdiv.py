import sys
import pymysql.cursors

import pandas as pd
import openpyxl
conn = pymysql.connect(
    read_default_file="~/.my.cnf",
    db='quest',  # db name
    charset='utf8',
    autocommit=True)
curs = conn.cursor()

for i in range(8020, 3659665): #3659665, 7841389
    try:
        cansmis = "select cansmi from compound where compound_id = %s; " % (i)
        curs.execute(cansmis)
        cansmi = curs.fetchall()
        chem_ids = "select chemdiv_id from chemdiv where compound_id = \
        (select compound_id from chemdiv_compound where cansmi = '%s'); " % (cansmi[0][0])
        curs.execute(chem_ids)
        chem_ids = curs.fetchall()
        #print(i, cid[0][0])
        
        new_chemdiv = "insert into original_chemdiv(compound_id, cansmi, chemdiv_id) values (%s, '%s', '%s'); " \
                        % (i, cansmi[0][0], chem_ids[0][0])

        curs.execute(new_chemdiv)
    except Exception as ex:
        print(i)

conn.close()
