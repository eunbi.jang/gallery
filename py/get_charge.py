"""
2021.06.10
Eunbi Jang
"""


import os
import pymysql.cursors
import math

conn = pymysql.connect(
    read_default_file="~/.my.cnf",
    db='quest',  # db name
    charset='utf8',
    autocommit=False
)

curs = conn.cursor()


for compound_id in range(3204198,3968006):
    print(compound_id)
    mol2s = "select mol2 from chemdiv_mol2 where compound_id = %s;" % compound_id 
    curs.execute(mol2s)
    mol2 = curs.fetchall()
    #print(mol2[0][0].split("@<TRIPOS>ATOM")[1])
    charges = 0

    try:
        mol2_string = mol2[0][0].split("@<TRIPOS>ATOM")[1].split("\n")
        for m_str in mol2_string:
            if "@" in m_str:
                break
            if len(m_str) > 0:
                length = len(m_str.split("    "))
                charges += float(m_str.split("    ")[length-1])
                #print(charges)
        charge = round(charges)
        #print(charge)
        
        charge_query = "update chemdiv_compound set charge = %s where compound_id = %s" %(charge, compound_id)
        curs.execute(charge_query)
    except Exception as e:
        print(e)


conn.commit()
conn.close()


