#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os 
import glob 
import trimesh
import numpy as np 
import tensorflow as tf 
from tensorflow import keras 
from tensorflow.keras import layers 
from matplotlib import pyplot as plt


# In[2]:


print("Num GPUs Available: ", len(tf.config.experimental.list_physical_devices('GPU')))

#physical_devices = tf.config.experimental.list_physical_devices('GPU')
#print("GPUs Available: ", len(physical_devices))

#tf.config.experimental.set_memory_growth(physical_devices, True)


# In[3]:


DATA_DIR = tf.keras.utils.get_file(
    "modelnet.zip",
    "https://3dvision.princeton.edu/projects/2014/3DShapeNets/ModelNet10.zip",
    extract=True,
)
DATA_DIR = os.path.join(os.path.dirname(DATA_DIR), "ModelNet10")


# In[76]:


folders = glob.glob(os.path.join(DATA_DIR, "[!README]*"))

classes = {folder: i for i,folder in enumerate(folders)}
classes


# In[77]:


def parse_dataset(num_points=2048):

    train_points = []
    train_labels = []
    test_points = []
    test_labels = []
    class_map = {}
    folders = glob.glob(os.path.join(DATA_DIR, "[!README]*"))

    for i, folder in enumerate(folders):
        print("processing class: {}".format(os.path.basename(folder)))
        # store folder name with ID so we can retrieve later
        class_map[i] = folder.split("/")[-1]
        # gather all files
        train_files = glob.glob(os.path.join(folder, "train/*"))
        test_files = glob.glob(os.path.join(folder, "test/*"))

        for f in train_files:
            train_points.append(trimesh.load(f).sample(num_points))
            train_labels.append(i)

        for f in test_files:
            test_points.append(trimesh.load(f).sample(num_points))
            test_labels.append(i)

    return (
        np.array(train_points),
        np.array(test_points),
        np.array(train_labels),
        np.array(test_labels),
        class_map,
    )
NUM_POINTS = 2048
NUM_CLASSES = 10
BATCH_SIZE = 32

train_points, test_points, train_labels, test_labels, CLASS_MAP = parse_dataset(
    NUM_POINTS
)


# In[27]:


len(train_points), len(test_points)


# In[28]:


len(train_labels), len(test_labels)


# In[31]:


CLASS_MAP[0]


# In[86]:


def rotate_point_cloud(points):
  rotated_data = np.zeros(points.shape, dtype=np.float32)

  for k in range(points.shape[0]):
      alpha = np.random.uniform() * 2 * np.pi
      beta = np.random.uniform() * 2 * np.pi
      gamma = np.random.uniform() * 2 * np.pi

      a_cos = np.cos(alpha)
      a_sin = np.sin(alpha)
      b_cos = np.cos(beta)
      b_sin = np.sin(beta)
      g_cos = np.cos(gamma)
      g_sin = np.sin(gamma)

      r = np.array([[b_cos, -(g_cos*b_sin), b_sin*g_sin],
            [a_cos*b_sin, (a_cos*b_cos*g_cos)-(a_sin*g_sin), -(g_cos*a_sin)-(a_cos*b_cos*g_sin)],
                [(a_sin*b_sin), (a_cos*g_sin)+(b_cos*g_cos*a_sin), (a_cos*g_cos)-(b_cos*a_sin*g_sin)]])
     
      a1_cos = np.cos(-gamma)
      a1_sin = np.sin(-gamma)
      b1_cos = np.cos(-beta)
      b1_sin = np.sin(-beta)
      g1_cos = np.cos(-alpha)
      g1_sin = np.sin(-alpha)

      r1 = np.array([[b1_cos, -(g1_cos*b1_sin), b1_sin*g1_sin],
            [a1_cos*b1_sin, (a1_cos*b1_cos*g1_cos)-(a1_sin*g1_sin), -(g1_cos*a1_sin)-(a1_cos*b1_cos*g1_sin)],
                [(a1_sin*b1_sin), (a1_cos*g1_sin)+(b1_cos*g1_cos*a1_sin), (a1_cos*g1_cos)-(b1_cos*a1_sin*g1_sin)]])

        
      shape_pc = points[k, ...]
      rotated_data[k, ...] += np.dot(shape_pc.reshape((-1, 3)), r)
      #rotated_data[k, ...] = np.dot(r, shape_pc.reshape((3, -1)))
      #print(np.dot(r,r1))
      
      #print("--------------------------------------")

  return rotated_data


# In[110]:


def augment(points, label):
    # jitter points
    points += tf.random.uniform(points.shape, -0.005, 0.005, dtype=tf.float32)
    # shuffle points
    points = tf.random.shuffle(points)
    return points, label


# In[111]:


train_points_rotate = rotate_point_cloud(train_points)
test_points_rotate = rotate_point_cloud(train_points)
train_dataset = tf.data.Dataset.from_tensor_slices((train_points_rotate, train_labels))
test_dataset = tf.data.Dataset.from_tensor_slices((test_points_rotate, test_labels))


# test_points_rotate = rotate_point_cloud(test_points)
train_dataset = train_dataset.shuffle(len(train_points_rotate)).map(augment).batch(BATCH_SIZE)
#train_dataset = train_dataset.shuffle(len(train_points)).map(augment).batch(BATCH_SIZE)
test_dataset = test_dataset.shuffle(len(test_points)).map(augment).batch(BATCH_SIZE)


# In[88]:


train_points_rotate = rotate_point_cloud(train_points)
len(train_points_rotate)


# In[ ]:


print(len(test_points))


# In[112]:


points = train_points_rotate[1]

fig = plt.figure(figsize=(5, 5))
ax = fig.add_subplot(111, projection="3d")
ax.scatter(points[:, 0], points[:, 1], points[:, 2])
ax.set_axis_off()
plt.show()


# In[ ]:


points = test_points[1]

fig = plt.figure(figsize=(5, 5))
ax = fig.add_subplot(111, projection="3d")
ax.scatter(points[:, 0], points[:, 1], points[:, 2])
ax.set_axis_off()
plt.show()


# In[94]:


def conv_bn(x, filters):
    x = layers.Conv1D(filters, kernel_size=1, padding="valid")(x)
    x = layers.BatchNormalization(momentum=0.0)(x)
    return layers.Activation("relu")(x)


def dense_bn(x, filters):
    x = layers.Dense(filters)(x)
    x = layers.BatchNormalization(momentum=0.0)(x)
    return layers.Activation("relu")(x)


# In[95]:


class OrthogonalRegularizer(keras.regularizers.Regularizer):
    def __init__(self, num_features, l2reg=0.001):
        self.num_features = num_features
        self.l2reg = l2reg
        self.eye = tf.eye(num_features)

    def __call__(self, x):
        x = tf.reshape(x, (-1, self.num_features, self.num_features))
        xxt = tf.tensordot(x, x, axes=(2, 2))
        xxt = tf.reshape(xxt, (-1, self.num_features, self.num_features))
        return tf.reduce_sum(self.l2reg * tf.square(xxt - self.eye))


# In[96]:


def tnet(inputs, num_features):

    # Initalise bias as the indentity matrix
    bias = keras.initializers.Constant(np.eye(num_features).flatten())
    reg = OrthogonalRegularizer(num_features)

    x = conv_bn(inputs, 32)
    x = conv_bn(x, 64)
    x = conv_bn(x, 512)
    x = layers.GlobalMaxPooling1D()(x)
    x = dense_bn(x, 256)
    x = dense_bn(x, 128)
    x = layers.Dense(
        num_features * num_features,
        kernel_initializer="zeros",
        bias_initializer=bias,
        activity_regularizer=reg,
    )(x)
    feat_T = layers.Reshape((num_features, num_features))(x)
    # Apply affine transformation to input features
    return layers.Dot(axes=(2, 1))([inputs, feat_T])


# In[97]:


inputs = keras.Input(shape=(NUM_POINTS, 3))

x = tnet(inputs, 3)
x = conv_bn(x, 32)
x = conv_bn(x, 32)
x = tnet(x, 32)
x = conv_bn(x, 32)
x = conv_bn(x, 64)
x = conv_bn(x, 512)
x = layers.GlobalMaxPooling1D()(x)
x = dense_bn(x, 256)
x = layers.Dropout(0.3)(x)
x = dense_bn(x, 128)
x = layers.Dropout(0.3)(x)

outputs = layers.Dense(NUM_CLASSES, activation="softmax")(x)

model = keras.Model(inputs=inputs, outputs=outputs, name="pointnet")
#model.summary()
#SVG(model_to_dot(model, show_shapes=True, dpi=65, ).create(prog='dot', format='svg'))


# In[114]:


model.compile(
    loss="sparse_categorical_crossentropy",
    optimizer=keras.optimizers.Adam(learning_rate=0.001),
    metrics=["sparse_categorical_accuracy"],
)
history = model.fit(train_dataset, epochs=200, validation_data=test_dataset)


# In[115]:


plt.plot(history.history['sparse_categorical_accuracy'])
plt.plot(history.history['val_sparse_categorical_accuracy'])
plt.title('Model accuracy')
plt.xlabel('Epoch')
plt.ylabel('Accuracy')
plt.legend(['Train', 'Test'], loc='upper left')
plt.show()


# In[102]:


plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('Model loss')
plt.xlabel('Epoch')
plt.ylabel('Loss')
plt.legend(['Train', 'Test'], loc='upper left')
plt.show()


# In[ ]:




