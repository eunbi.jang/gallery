'''
2021.02.18
Eunbi Jang
'''

import argparse
import os
import subprocess
import pymysql.cursors


idx = 0

path_dir = '/home/eunbijang/zinc'
file_list = os.listdir(path_dir)
# connection 정보
conn = pymysql.connect(
    read_default_file="~/.my.cnf",
    db='quest',  # db name
    charset='utf8',
    autocommit=False)

curs = conn.cursor()


for i, list in enumerate(file_list):
    unique_id = list.split('.')[0]

    get_comp = "select compound_id from unique_id where unique_id = '%s';" % (unique_id)
    curs.execute(get_comp)
    compound_id = curs.fetchall()

    cmd = 'obabel -i mol2 -o can %s' % ("/home/eunbijang/zinc/" + list)
    cansmi = subprocess.check_output(cmd.split()).decode('utf-8')
    print(cansmi)
    sql = "update merge set cansmi2 = '%s' where Compound_id = %s;" % (cansmi.split()[0], compound_id[0][0])
    curs.execute(sql)
    
conn.close()
