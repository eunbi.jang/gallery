'''
2021.02.22
Eunbi Jang
'''


import os
import pymysql.cursors

conn = pymysql.connect(
    read_default_file="~/.my.cnf",
    db='quest',  # db name
    charset='utf8',
    autocommit=False)
curs = conn.cursor()

#987937
for compound_id in range(1481905,2609947):
    with open('Enamine_hts_collection_202007.smiles', 'r') as f:
        text = f.readlines()
        #print(text[compound_id-493969])
        sql = 'update merge set cansmi2 = "%s" where Compound_id = %s ' % (text[compound_id-493969], compound_id)
        curs.execute(sql)

    

conn.close()

    
