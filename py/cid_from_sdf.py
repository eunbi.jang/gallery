import subprocess
import signal
import time
import errno
import os
import functools

FILE_PATH = "/home/eunbijang/gallery/1711266661803743625.sdf"

def timeout(seconds=10, error_message=os.strerror(errno.ETIME)):
    def decorator(func):
        def _handle_timeout(signum, frame):
            raise TimeoutError(error_message)

        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            signal.signal(signal.SIGALRM, _handle_timeout)
            signal.alarm(seconds)
            try:
                result = func(*args, **kwargs)
            finally:
                signal.alarm(0)
            return result

        return wrapper
    return decorator

@timeout(2)
def get_mol2(sdf):
    sdf_path = "/home/eunbijang/gallery/1711266661803743625_sdf/%s.sdf" % (sdf.split()[1])
    with open(sdf_path, "w") as fs:
        fs.write(sdf) 
    cmd = "obabel -i sdf -o mol2 %s --gen3d" % (sdf_path)
    mol2 = subprocess.check_output(cmd.split()).decode('utf-8')

    return mol2
    
    


with open(FILE_PATH,"r") as f:
    sdfs = f.read() # read whole text as string
    sdf = sdfs.split("$$$$\n")
    for j,i in enumerate(sdf):
        try:
            mol2 = get_mol2(i)
            print(i.split()[0])
            file_name = "/home/eunbijang/gallery/mol2_3d/%s.mol2" % (i.split()[0])
            with open(file_name, "w") as f:
                f.write(mol2)
        except TimeoutError:
            continue



