import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--list', '-l', type=str, required=True)
args = parser.parse_args()

# file_path = '/home/eunbijang/version'
# file_list = os.listdir(file_path)
atom_dict = {}
global num_n 
global num_c 
global num_o 

coord_N = []
c_list = []
def check_atom(atom_list):
    num_n = 0
    num_c = 0
    num_o = 0
    conn_list = []
    for i,line in enumerate(atom_list):
        if i > 4:
            atom = line.split()
            if len(atom) == 16:
                # key error때문에 일단 dict 에 다 넣어줌
                atom_dict[i-4] = atom[3] # 라인넘버와 어떤 원자인지 딕셔너리에 넣어줌
                if atom[3] == 'N':# N일경우
                    num_n +=1
                    coord_N.append(i-3)
                elif atom[3] == 'C':# C일경우
                    num_c +=1
                elif atom[3] == 'O':# O일 경우
                    num_o +=1

            elif len(atom) == 7  and atom[0].isdigit():
                if not(num_n >0 and num_c >1 and num_o >1): # N-C-C-OO 가 될 수 있는 최소한의 원자갯수가 안될경우
                    break # amino acid 아님
                elif (num_n >0 and num_c >1 and num_o >1):
                    conn = [atom[0], atom[1], atom[2]] # 연결정보와 본딩인포를 리스트에 담아준후
                    conn_list.append(conn) # 모든 연결정보들의 리스트에 append해줌 
                    # 아미노산이 아닐경우 conn_list 는 비어있어야함
    return (atom_dict, conn_list, coord_N)

def n2c(atom_dict, connection, n_number):
    c_list = []
    idx = 0

    while idx < len(connection): #연결정보에서 N이랑 이어진 부분들
        num = connection[idx]
        if int(num[0]) == n_number: 
            conn_number = int(num[1])
            if int(num[2]) == 1 and atom_dict[conn_number] == 'C': # N-C는 싱글본드만 됨
                c_list.append(conn_number) #N-C인경우 C의 번호 저장 
                connection.pop(idx) #연결정보에서 사용한부분은 삭제 
            else:
                idx += 1

        elif int(num[1]) == n_number:
            conn_number = int(num[0])
            if int(num[2]) == 1 and atom_dict[conn_number] == 'C': 
                c_list.append(conn_number) #N-C인경우 C의 번호 저장 
                connection.pop(idx) #연결정보에서 사용한부분은 삭제 
            else:
                idx += 1
        else:
            idx += 1
    # N-C 로 이어진 부분이 있다면 
    
    if c_list:
        nc = c2c(atom_dict, connection, c_list)
        if nc:
            return True
    else:
        return False

def c2c(atom_dict, connection, c_number):
    
    c_list = []
    
    for i,c_num in enumerate(c_number): #N-C인 경우의수 
        idx = 0
        while idx < len(connection):#남은 연결정보들
            num = connection[idx]
            if int(num[0]) == c_num: #C 와 연결된경우
                conn_number = int(num[1]) # C 와 연결된 부분의 번호
                if int(num[2]) == 1 and atom_dict[conn_number] == 'C': # C-C
                    c_list.append(conn_number)
                    connection.pop(idx) #연결정보에서 사용한부분은 삭제
                else:
                    idx += 1
            elif int(num[1]) == c_num:
                conn_number = int(num[0])
                if int(num[2]) == 1 and atom_dict[conn_number] == 'C': # C-C
                    c_list.append(conn_number)
                    connection.pop(idx) #연결정보에서 사용한부분은 삭제
                else:
                    idx += 1
            else :
                idx += 1        
    
    if c_list: # C-C로 이어진 부분이 있다면 
        ncc = c2o(atom_dict, connection, c_list)
        if ncc:
            return True
    else: 
        return False

def c2o(atom_dict, connection, c_list):
    cnt  = 0 #O는 두개가 있어야함 
    
    for i,c_num in enumerate(c_list): #N-C인 경우의수 
        idx = 0
        while idx < len(connection): #남은 연결정보들
            num = connection[idx]
            if int(num[0]) == c_num: #C 와 연결된경우
                conn_number = int(num[1]) # C 와 연결된 부분의 번호
                if atom_dict[conn_number] == 'O': # C-O
                    cnt += 1 # O갯수 세주기 
                    connection.pop(idx) #연결정보에서 사용한부분은 삭제
                else:
                    idx += 1

            elif int(num[1]) == c_num:
                conn_number = int(num[0])
                if (str(conn_number) in atom_dict) and atom_dict[conn_number] == 'O': # C-O
                    cnt += 1 # O갯수 세주기 
                    connection.pop(idx) #연결정보에서 사용한부분은 삭제
                else:
                    idx += 1
            else:
                idx += 1
        
        if cnt == 2:# C-OO 인경우
            #print("umm")
            return True
        else:
            cnt = 0

with open(args.list, 'r') as f:

    sdf_file = f.read()
    sdfs = sdf_file.split('$$$$')

    for i, sdf in enumerate(sdfs):
        
        sd = sdf.split('M  END')[0]
        sd_list = sd.split('\n')
        cno_list, connection, n_number =  check_atom(sd_list)
        if connection:# 넘길때 for loop n인 좌표들 하나씩 넘겨줌
            for n_num in n_number:
                amino_acid = n2c(cno_list, connection, n_num)
                if amino_acid: #N-C-C-OO 를 반환받았을때
                    print(args)
                    fr = open('/home/eunbijang/amino_acid/' + args+ '.sdf', 'a')
                    fr.write(sdf)
                    fr.write('$$$$')

    fr.close()
    
  

