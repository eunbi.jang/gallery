"""
Usage: python3 pubchem.py

Output:

Authored by Eunbi Jang
Created: 06-29-2021

Copyright (c) 2021 Quantum Intelligence Corporation
"""
from pubchem_rest import fetch_smiles_with_pubchem_id
import pandas as pd
import openpyxl
import pymysql.cursors

conn = pymysql.connect(
    read_default_file="~/.my.cnf",
    db='quest',  # db name
    charset='utf8',
    autocommit=False
)

curs = conn.cursor()

pampa = pd.read_excel('/home/eunbijang/pampa_chemonly.xlsx', usecols = [1], header = None)
df = pd.read_excel('/home/eunbijang/pampa_chemonly.xlsx', header = None)
# for i in range(1, len(pampa)):
#     try:
#         cid = str(pampa.loc[i]).split()[1]
#         print(cid)
#         smiles = fetch_smiles_with_pubchem_id(cid)
#         print(smiles)
#         sql = "insert into pampa(pubchem_id, smiles) values('%s', '%s'); " % (cid, smiles)
#         curs.execute(sql)
#     except Exception as ex:
#         print(ex)
smiles_list = []
for i in range(0, len(pampa)):
    try:
        cid = str(pampa.loc[i]).split()[1]
        print(cid)
        sql = "select smiles from pampa where pubchem_id = '%s' ; " % (cid)
        curs.execute(sql)
        smiles = curs.fetchall()
        
        if smiles:
            smiles_list.append(smiles[0][0])
        else:
            smiles_list.append('')
            print("NULL")
    except Exception as ex:
        print(ex)

    df[15] = pd.DataFrame(smiles_list)
    df.to_excel('/home/eunbijang/new_pampa_chemonly.xlsx')

conn.commit()
conn.close()
