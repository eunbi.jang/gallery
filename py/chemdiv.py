import sys
import pymysql.cursors

import pandas as pd
import openpyxl
conn = pymysql.connect(
    read_default_file="~/.my.cnf",
    db='quest',  # db name
    charset='utf8',
    autocommit=True)
curs = conn.cursor()

cid = pd.read_excel('/home/eunbijang/chemdiv.xlsx', usecols = [0], header = None)
df = pd.read_excel('/home/eunbijang/chemdiv.xlsx', header = None)

chemdiv_list = []
for i in range(0, len(cid)):
    try:
        compound_id = str(cid.loc[i]).split()[1]
        sql = "select cansmi from compound where compound_id = %s; " % (int(compound_id))
        curs.execute(sql)
        smiles = curs.fetchall()
        
        if smiles: #cansmi
            chemdiv_ids = "select chemdiv_id from chemdiv where compound_id = \
            (select compound_id from chemdiv_compound where cansmi = '%s');" % (smiles[0][0])
            curs.execute(chemdiv_ids)
            chemid = curs.fetchall()
            print(compound_id, chemid[0][0])
            chemdiv_list.append(chemid[0][0])
        else:
            print(compound_id)
    except Exception as ex:
        chemdiv_list.append(ex)
        print(ex)

    df[2] = pd.DataFrame(chemdiv_list)
    df.to_excel('/home/eunbijang/chemdiv2.xlsx')


conn.close()