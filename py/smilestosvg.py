"""
Authored by Eunbi Jang
Created: 03-30-2021
"""
import os
import subprocess
import pymysql.cursors
from draw2dimg import draw_svg_with_smiles

# connection 정보
conn = pymysql.connect(
    read_default_file="~/.my.cnf",
    db='quest',  # db name
    charset='utf8',
    autocommit=False)

curs = conn.cursor()

for compound_id in range(2759847, 2779847):
    print(compound_id)
    smiles_sql = 'select smiles from compound where compound_id = %s;' % (compound_id)
    curs.execute(smiles_sql)
    smiles = curs.fetchall()
    
    #print(svg)
    try:
        svg = draw_svg_with_smiles(smiles[0][0])
        svg_sql = 'insert into svg(compound_id, svg) values(%s, "%s");' % (compound_id, svg)
        curs.execute(svg_sql)
    except:
        svg_sql = 'insert into svg(compound_id) values(%s);' % (compound_id)
        curs.execute(svg_sql)


conn.commit()
conn.close()






