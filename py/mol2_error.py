'''
2021.04.10
Eunbi Jang
'''


import os
import pymysql.cursors
import subprocess

conn = pymysql.connect(
    read_default_file="~/.my.cnf",
    db='quest',  # db name
    charset='utf8',
    autocommit=False
)

curs = conn.cursor()


err_nums = 'select compound_id, cansmi from compound where not exists\
    ( select mol2 from mol2 where compound.compound_id = mol2.compound_id ) and compound.compound_id > 3978984;'
curs.execute(err_nums)
mol2_error = curs.fetchall()
print(len(mol2_error))
print(mol2_error[0])
print(mol2_error[1][0])
#with open('mol2_convert_error.txt', 'a') as f:
    #f.write(mol2_error[0][0])
    
conn.commit()
conn.close()