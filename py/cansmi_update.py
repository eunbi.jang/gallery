"""
remove '\n' in cansmi and update value

Authored by Eunbi Jang
Created: 26-04-2021

Copyright (c) 2021 Quantum Intelligence Corporation
"""

import os
import pymysql.cursors

conn = pymysql.connect(
    read_default_file="~/.my.cnf",
    db='quest',  # db name
    charset='utf8',
    autocommit=False)
curs = conn.cursor()


for compound_id in range(1,2759847):#2759846까지만 들어감 그후는 cansmi가 없는듯
    print(compound_id)
    cansmi_sql = 'select cansmi from compound where compound_id = %s' % (compound_id)
    curs.execute(cansmi_sql)
    cansmi_tuple = curs.fetchall()
    #print(cansmi_tuple[0][0])
    cansmi = cansmi_tuple[0][0]
    sql = 'update compound set cansmi = "%s" where compound_id = %s ' % (cansmi.split()[0], compound_id)
    #print(sql)
    curs.execute(sql)

conn.commit()
conn.close()

