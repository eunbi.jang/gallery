""" check pubchem id with smiles

Usage: python3 pubchem.py

Output:

Authored by Eunbi Jang
Created: 06-30-2021

Copyright (c) 2021 Quantum Intelligence Corporation
"""
from pubchem_rest import fetch_pubchemid_with_smiles
import pandas as pd
import pymysql.cursors
import time

conn = pymysql.connect(
    read_default_file="~/.my.cnf",
    db='quest',  # db name
    charset='utf8',
    autocommit=False
)

curs = conn.cursor()

pampa = pd.read_excel('/home/eunbijang/pampa_chemonly.xlsx', usecols = [1], header = None)

for i in range(0, len(pampa)):
    if i >10:
        try:
            cid = str(pampa.loc[i]).split()[1]
            sql = "select smiles from pampa where pubchem_id = '%s' ; " % (cid)
            curs.execute(sql)
            smiles = curs.fetchall()
            print(cid)

            if smiles:
                pubchem_id, _ = fetch_pubchemid_with_smiles(smiles[0][0])
                print(pubchem_id)
                if not cid in pubchem_id:
                    print(smiles[0][0])
                    print(pubchem_id)
                # if pubchem_id[0] != cid:
                #     print(pubchem_id[0])
                #     print(smiles[0][0])
                #     #print(cid)

        except Exception as ex:
            print("Error: ", ex)

conn.commit()
conn.close()


