import sys
import numpy as np
import struct
import re
from io import StringIO

BOHR = 0.529177


class Atom:
    """Atom class"""
    serial = 1
    charge = 0
    position = [0, 0, 0]
    atom_type = 'H'
    chain = 'A'
    aname = ''
    resname = 'M1'
    neighbors = []
    bond_orders = []
    mol_type = 'Protein'

    def __init__(self, charge, position, atom_type, chain, aname, resname, resid, mol_type='Protein', serial=1):
        self.charge = charge
        self.position = position
        self.atom_type = atom_type
        self.chain = chain
        self.aname = aname
        self.resname = resname
        self.resid = resid
        self.neighbors = []
        self.bond_orders = []
        self.mol_type = mol_type
        self.serial = serial

    def distance_sq(self, position):
        ret = 0
        for a, b in zip(self.position, position):
            ret += (a-b)**2

        return ret

    @property
    def element_type(self):
        return re.sub(r'[0-9|\.].*$', '', self.atom_type).capitalize()

    @property
    def position_np(self):
        return np.array(self.position)


def kabsch_rotate(P, Q):
    """
    Rotate matrix P unto matrix Q using Kabsch algorithm.

    Parameters
    ----------
    P : array
        (N,D) matrix, where N is points and D is dimension.
    Q : array
        (N,D) matrix, where N is points and D is dimension.

    Returns
    -------
    P : array
        (N,D) matrix, where N is points and D is dimension,
        rotated

    """
    U = kabsch(P, Q)

    # Rotate P
    P = np.dot(P, U)
    return P


def kabsch(P, Q):
    """
    The optimal rotation matrix U is calculated and then used to rotate matrix
    P unto matrix Q so the minimum root-mean-square deviation (RMSD) can be
    calculated.

    Using the Kabsch algorithm with two sets of paired point P and Q, centered
    around the centroid. Each vector set is represented as an NxD
    matrix, where D is the the dimension of the space.

    The algorithm works in three steps:
    - a translation of P and Q
    - the computation of a covariance matrix C
    - computation of the optimal rotation matrix U

    http://en.wikipedia.org/wiki/Kabsch_algorithm

    Parameters
    ----------
    P : array
        (N,D) matrix, where N is points and D is dimension.
    Q : array
        (N,D) matrix, where N is points and D is dimension.

    Returns
    -------
    U : matrix
        Rotation matrix (D,D)

    Example
    -----
    TODO

    """

    # Computation of the covariance matrix
    C = np.dot(np.transpose(P), Q)

    # Computation of the optimal rotation matrix
    # This can be done using singular value decomposition (SVD)
    # Getting the sign of the det(V)*(W) to decide
    # whether we need to correct our rotation matrix to ensure a
    # right-handed coordinate system.
    # And finally calculating the optimal rotation matrix U
    # see http://en.wikipedia.org/wiki/Kabsch_algorithm
    V, S, W = np.linalg.svd(C)
    d = (np.linalg.det(V) * np.linalg.det(W)) < 0.0

    if d:
        S[-1] = -S[-1]
        V[:, -1] = -V[:, -1]

    # Create Rotation matrix U
    U = np.dot(V, W)

    return U


def read_pdbfile(pdbfile):
    """
    ##############################################################################################
    #   Record Format
    #
    #   COLUMNS        DATA  TYPE    FIELD        DEFINITION
    #   -------------------------------------------------------------------------------------
    #    1 -  6        Record name   "ATOM  "
    #    7 - 11        Integer       serial       Atom  serial number.
    #   13 - 16        Atom          name         Atom name.
    #   17             Character     altLoc       Alternate location indicator.
    #   18 - 20        Residue name  resName      Residue name.
    #   22             Character     chainID      Chain identifier.
    #   23 - 26        Integer       resSeq       Residue sequence number.
    #   27             AChar         iCode        Code for insertion of residues.
    #   31 - 38        Real(8.3)     x            Orthogonal coordinates for X in Angstroms.
    #   39 - 46        Real(8.3)     y            Orthogonal coordinates for Y in Angstroms.
    #   47 - 54        Real(8.3)     z            Orthogonal coordinates for Z in Angstroms.
    #   55 - 60        Real(6.2)     occupancy    Occupancy.
    #   61 - 66        Real(6.2)     tempFactor   Temperature  factor.
    #   77 - 78        LString(2)    element      Element symbol, right-justified.
    #   79 - 80        LString(2)    charge       Charge  on the atom.
    ##############################################################################################
    """
    mols = []
    atoms = []
    molnames = []
    molname = ""
    with open(pdbfile, 'r') as f:
        for line in f:
            if line[:4] == "ATOM" or line[:6] == "HETATM":
                #                position = list(map(float, line[30:54].split()))
                position = list(
                    map(float, [line[30:38], line[38:46], line[46:54]]))
                resno = int(line[22:26])
                altloc = line[16]
                charge = float(line[60:66])
                atype = line[76:79].strip()
                chain = line[21:23].strip()
                resname = line[17:20].strip()
                aname = line[12:16].strip()
                mol_type = "Protein" if line[:4] == "ATOM" else "Ligand"

                if altloc in (' ', 'A', '1', '2', '3', '4'):
                    if altloc in ('1', '2', '3', '3'):
                        aname += altloc
                    atom = Atom(charge, position, atype,
                                chain, aname, resname, resno, mol_type)
                    atom.resno = resno

                    atoms.append(atom)
            elif line[:20] == "REMARK MOLECULE NAME":
                tokens = line[21:].split()

                for token in tokens:
                    molname = token.split(":")
                    molnames.append(molname[0])

            elif "COMPND    " in line:
                if not molname:
                    cid = line[7:].strip()
                    molnames.append(cid)
                    molname = cid
            elif "REMARK  Name = " in line:
                if not molname:
                    cid = line[14:].strip()

                    if cid:
                        molnames.append(cid)
                        molname = cid
            elif "ENDMDL" in line:
                if not molname:
                    molname = "UNKNOWN"
                    molnames.append(molname)
                mols.append(atoms)
                atoms = []
                molname = ""
        if atoms:
            if molname:
                molnames.append("UNKNOWN")
            mols.append(atoms)

    return mols, molnames


def write_pdbfile(mols, molnames, outstream=sys.stdout, protein_only=False):
    for mol, molname in zip(mols, molnames):
        print("REMARK MOLECULE NAME %s" % (molname), file=outstream)
        print("MODEL        1", file=outstream)
        for (ai, atom) in enumerate(mol):
            mol_type = "ATOM  " if atom.mol_type == "Protein" else "HETATM"
            if not protein_only or mol_type == "ATOM  ":
                print("%6s" % (mol_type), end='', file=outstream)
                print("%5d" % (ai+1), end='', file=outstream)
                print("  ", end='', file=outstream)
                print("%-4s" % (atom.aname), end='', file=outstream)
                print("%-3s " % (atom.resname[:3]), end='', file=outstream)
                print("%1s" % (atom.chain), end='', file=outstream)
                print("%4d" % (atom.resid), end='', file=outstream)
                print("    ", end='', file=outstream)
                print("%8.3f" % (atom.position[0]), end='', file=outstream)
                print("%8.3f" % (atom.position[1]), end='', file=outstream)
                print("%8.3f" % (atom.position[2]), end='', file=outstream)
                print("%6.2f" % (0.0), end='', file=outstream)
                print("%6.2f" % (0.0), end='', file=outstream)
                print("  ", end='', file=outstream)
                print("  ", end='', file=outstream)
                print("%6s" % (""), end='', file=outstream)
                print("%2s" % (atom.element_type), end='\n', file=outstream)

        print("ENDMDL", file=outstream)


def write_pdbqfile(mols, molnames, outstream=sys.stdout):
    for mol in mols:
        print("MODEL        1", file=outstream)
        for (ai, atom) in enumerate(mol):
            atom.resname = re.sub(r'[0-9]', '', atom.resname)
            print("ATOM  ", end='', file=outstream)
            print("%5d" % (ai+1), end='', file=outstream)
            print("  ", end='', file=outstream)
            print("%-4s" % (atom.aname), end='', file=outstream)
            print("%-3s " % (atom.resname), end='', file=outstream)
            print("%1s" % (atom.chain), end='', file=outstream)
            print("%4d" % (atom.resid), end='', file=outstream)
            print("    ", end='', file=outstream)
            print("%8.3f" % (atom.position[0]), end='', file=outstream)
            print("%8.3f" % (atom.position[1]), end='', file=outstream)
            print("%8.3f" % (atom.position[2]), end='', file=outstream)
            print("%6.2f" % (0.0), end='', file=outstream)
            print("%6.2f" % (0.0), end='', file=outstream)
            print("  ", end='', file=outstream)
            print("  ", end='', file=outstream)
            print("%6.3f " % (atom.charge), end='', file=outstream)
            print("%-4s" % (atom.atom_type), end='\n', file=outstream)

        print("ENDMDL", file=outstream)


def read_mol2(file):
    mols = []
    molnames = []
    while(True):
        atoms = []
        molname = ""
        title = file.readline()

        if not title:
            break

        molname = file.readline().strip()
        tokens = list(map(int, file.readline().split()))
        natoms = tokens[0]
        nbonds = tokens[1]

        type = file.readline().strip()
        charge_type = file.readline().strip()
        file.readline()

        # Header for atoms
        file.readline()
        for _ in range(natoms):
            line = file.readline()
            tokens = line.split()

            serial = int(tokens[0])
            position = list(map(float, tokens[2:5]))
            resno = 1
            charge = float(tokens[8])
            atype = tokens[5].strip()
            chain = 'A'
            resname = 'UNL1'
            aname = tokens[1].strip()
            atom = Atom(charge, position, atype,
                        chain, aname, resname, resno, serial=serial)
            atom.resno = resno
            atom.resname = tokens[7].strip()
            atoms.append(atom)

        # Header for bonds
        while (True):
            line = file.readline()
            if '@<TRIPOS>BOND' in line:
                break

        for _ in range(nbonds):
            line = file.readline()
            tokens = line.split()
            ai = int(tokens[1])-1
            aj = int(tokens[2])-1
            bond_order = tokens[3]
            atoms[ai].neighbors.append(aj)
            atoms[aj].neighbors.append(ai)
            atoms[ai].bond_orders.append(bond_order)
            atoms[aj].bond_orders.append(bond_order)

        mols.append(atoms)
        molnames.append(molname)
    return mols, molnames


"""
Read and parse mol2 text from a string variable and return Atom objects and molnames.
"""


def read_mol2str(mol2str):
    f = StringIO(mol2str)
    return read_mol2(f)


"""
Read and parse mol2 text from a text for a given mol2 file name and return Atom objects and molnames.
"""


def read_mol2file(mol2file):
    with open(mol2file, 'r') as f:
        return read_mol2(f)


def write_mol2str(stream, mols, molnames):
    """Write a mol2 file to stream with mols and molnames.
    """
    for (atoms, molname) in zip(mols, molnames):
        bonds = []
        natoms = len(atoms)
        for ai in range(natoms):
            for aj, bond_order in zip(atoms[ai].neighbors, atoms[ai].bond_orders):
                if ai < aj:
                    bonds.append([ai, aj, bond_order])
        nbonds = len(bonds)

        stream.write("@<TRIPOS>MOLECULE\n")
        stream.write("%s\n" % (molname))
        stream.write(" %d %d 0 0 0\n" % (natoms, nbonds))
        stream.write("SMALL\n")
        stream.write("GASTEIGER\n")
        stream.write("\n")
        stream.write("@<TRIPOS>ATOM\n")
        for ai in range(natoms):
            stream.write("%7d %-8s %9.4f %9.4f %9.4f %-5s %3d  %-5s %12.4f\n" % (
                ai+1, atoms[ai].aname, atoms[ai].position[0], atoms[ai].position[1], atoms[ai].position[2], atoms[ai].atom_type, atoms[ai].resid, atoms[ai].resname, atoms[ai].charge))

        stream.write("@<TRIPOS>BOND\n")
        for bi, bond in enumerate(bonds):
            stream.write("%6s %5d %5d %4s\n" %
                    (bi+1, bond[0]+1, bond[1]+1, bond[2]))

def write_mol2file(mol2file, mols, molnames):
    """Write a mol2 file with mols and molnames.
    """
    with open(mol2file, 'w') as f:
        write_mol2str(f, mols, molnames)

def read_dat(filename):
    with open(filename, 'rb') as f:
        ngrids = []
        for _ in range(3):
            ngrid, = struct.unpack('l', f.read(8))
            ngrids.append(ngrid)

        tot_ngrid = ngrids[0]*ngrids[1]*ngrids[2]
        dh, = struct.unpack('f', f.read(4))
        dh *= BOHR

        origin = []

        boundary = [[0, 0] for x in range(3)]
        for d in range(3):
            boundary[d][0], = struct.unpack('f', f.read(4))
            boundary[d][1], = struct.unpack('f', f.read(4))
            boundary[d][0] *= BOHR
            boundary[d][1] *= BOHR
            origin.append(boundary[d][0])

        values = np.fromfile(f, np.float32, tot_ngrid)
        delta = [dh, dh, dh]

        return ngrids, origin, delta, values


def write_dat(ngrids, origin, delta, values, outdat):
    for d in range(3):
        packed = struct.pack('l', ngrids[d])
        outdat.write(packed)

    packed = struct.pack('f', delta[0]/BOHR)

    outdat.write(packed)

    boundary = [[0, 0] for x in range(3)]
    for d in range(3):
        boundary[d][0] = origin[d]
        boundary[d][1] = origin[d] + ngrids[d]*delta[d]

        packed = struct.pack('f', boundary[d][0]/BOHR)
        outdat.write(packed)

        packed = struct.pack('f', boundary[d][1]/BOHR)
        outdat.write(packed)

    values *= BOHR
    values.tofile(outdat, '', 'f')


def read_dx(filename):
    with open(filename, 'r') as f_dx:
        f_dx.readline()  # Title line
        object1_line = f_dx.readline()
        origin_line = f_dx.readline()
        deltax_line = f_dx.readline()
        deltay_line = f_dx.readline()
        deltaz_line = f_dx.readline()
        f_dx.readline()  # object line2
        f_dx.readline()  # object line3

        ngrids = list(map(int, object1_line.split()[5:8]))
        origin = list(map(float, origin_line.split()[1:4]))
        delta = (float(deltax_line.split()[1]), float(
            deltay_line.split()[2]), float(deltaz_line.split()[3]))

        values = []

        for _ in range(ngrids[0]):
            for _ in range(ngrids[1]):
                for _ in range(ngrids[2]):
                    data_line = f_dx.readline()
                    data = float(data_line)

                    value = data

    #                print (value)
                    values.append(value)
                f_dx.readline()
            f_dx.readline()

    return ngrids, origin, delta, values


def write_dx(ngrids, origin, delta, values, out):
    out.write("#data\n")
    out.write("object 1 class gridpositions counts %d %d %d\n" %
              (ngrids[0], ngrids[1], ngrids[2]))
    out.write("origin %.3f %.3f %.3f\n" % (origin[0], origin[1], origin[2]))
    out.write("delta %.3f 0.0 0.0\n" % (delta[0]))
    out.write("delta 0.0 %.3f 0.0\n" % (delta[1]))
    out.write("delta 0.0 0.0 %.3f\n" % (delta[2]))
    out.write("object 2 class gridconnections counts %d %d %d\n" %
              (ngrids[0], ngrids[1], ngrids[2]))
    out.write("object 3 class array type double rank 0 times %d\n" %
              (ngrids[0]*ngrids[1]*ngrids[2]))

    ii = 0
    for ix in range(ngrids[0]):
        for iy in range(ngrids[1]):
            for iz in range(ngrids[2]):
                out.write("%g\n" % (values[ii]))
                ii += 1
            out.write("\n")
        out.write("\n")


def read_logfile(logfile, heavy_only=False):
    with open(logfile, 'r') as fin:
        state = 'none'

        atoms = []
        charges = []
        for line in fin:
            if (not heavy_only and 'ESP charges:' in line) or (heavy_only and 'ESP charges with hydrogens summed' in line):
                fin.readline()
                state = 'read'
            elif state == 'read':
                if 'Sum of ESP charges' in line or 'Charge= ' in line:
                    return atoms, charges
                else:
                    tokens = line.split()
                    atom = tokens[1]
                    charge = float(tokens[2])

                    atoms.append(atom)
                    charges.append(charge)

        return atoms, charges


def read_grofile(grofile, heavy_only=False):
    with open(grofile, 'r') as fin:
        molnames = (fin.readline())
        atoms = []
        natoms = int(fin.readline().strip())

        for _ in range(natoms):
            line = fin.readline()
            resno = int(line[:5].strip())
            resname = line[5:8].strip()
            aname = line[8:15].strip()
#            index = int(line[15:20].strip())
            x = float(line[20:28].strip())*10
            y = float(line[28:36].strip())*10
            z = float(line[36:44].strip())*10
            position = (x, y, z)
            charge = 0.0
            atype = re.sub(r'[0-9]', '', aname)[0]
            chain = 'A'
            atom = Atom(charge, position, atype, chain, aname, resname, resno)

            if not heavy_only or aname[0] != "H":
                atoms.append(atom)

        return [atoms], [molnames]


def read_wfnfile(wfnfile):
    with open(wfnfile, 'r') as fin:
        molnames = [fin.readline()]
        atoms = []

        line = fin.readline()
        natoms = int(line.split()[6])

        for _ in range(natoms):
            line = fin.readline()
            tokens = line.split()
            resno = 1
            resname = 'UNK'
            aname = tokens[0]
            atype = tokens[0]
            x = float(tokens[4])
            y = float(tokens[5])
            z = float(tokens[6])
            position = (x, y, z)
            chain = 'A'
            charge = 0.0
            atom = Atom(charge, position, atype, chain, aname, resname, resno)

            atoms.append(atom)

        return [atoms], molnames


def read_atomlines(f, mol, molname):
    atom_properties = []
    for line in f:
        if re.search(r'^[ \t]*$', line):
            break

        if len(mol) <= len(atom_properties):
            break

        line = re.sub(r';.*', '', line)

        # Only process a line when it is not a white-space-only line.
        if not re.search(r'^[ \t]*$', line):
            tokens = line.split()

            try:
                atom_type = tokens[1]
                charge = float(tokens[6])
            except:
                print("Error line: %s", line)

            atom_properties.append((atom_type, charge))

    for a, p in zip(mol, atom_properties):
        a.atom_type = p[0]
        a.charge = p[1]


def read_topfile(filename, mols, molnames):
    with open(filename, 'r') as f:
        for line in f:
            # Remove comments
            line = re.sub(r';.*', '', line)

            if not re.search(r'^[ \t]*$', line):
                if '[ atoms ]' in line:
                    read_atomlines(f, mols, molnames)


def center_of_geom(atoms):
    gm = [0.0, 0.0, 0.0]
    natoms = len(atoms)

    for atom in atoms:
        for d in range(len(atom.position)):
            gm[d] += atom.position[d]

    for d in range(len(gm)):
        gm[d] /= natoms

    return gm


def center_of_mass(atoms):
    cm = [0.0, 0.0, 0.0]

    atomic_masses = {"H": 1.008000, "He": 4.002602, "Li": 6.940000, "Be": 9.012183, "B": 10.810000, "C": 12.011000, "N": 14.007000, "O": 15.999000, "F": 18.998403, "Ne": 20.179700, "Na": 22.989769, "Mg": 24.305000, "Al": 26.981538, "Si": 28.085000, "P": 30.973762, "S": 32.060000, "Cl": 35.450000, "Ar": 39.948000, "K": 39.098300, "Ca": 40.078000, "Sc": 44.955908, "Ti": 47.867000, "V": 50.941500, "Cr": 51.996100, "Mn": 54.938044, "Fe": 55.845000, "Co": 58.933194, "Ni": 58.693400, "Cu": 63.546000, "Zn": 65.380000, "Ga": 69.723000, "Ge": 72.630000, "As": 74.921595, "Se": 78.971000, "Br": 79.904000, "Kr": 83.798000, "Rb": 85.467800, "Sr": 87.620000, "Y": 88.905840, "Zr": 91.224000, "Nb": 92.906370, "Mo": 95.950000, "Tc": 97.907210, "Ru": 101.070000, "Rh": 102.905500, "Pd": 106.420000, "Ag": 107.868200, "Cd": 112.414000, "In": 114.818000, "Sn": 118.710000, "Sb": 121.760000, "Te": 127.600000, "I": 126.904470, "Xe": 131.293000, "Cs": 132.905452, "Ba": 137.327000, "La": 138.905470, "Ce": 140.116000, "Pr": 140.907660, "Nd": 144.242000,
                     "Pm": 144.912760, "Sm": 150.360000, "Eu": 151.964000, "Gd": 157.250000, "Tb": 158.925350, "Dy": 162.500000, "Ho": 164.930330, "Er": 167.259000, "Tm": 168.934220, "Yb": 173.045000, "Lu": 174.966800, "Hf": 178.490000, "Ta": 180.947880, "W": 183.840000, "Re": 186.207000, "Os": 190.230000, "Ir": 192.217000, "Pt": 195.084000, "Au": 196.966569, "Hg": 200.592000, "Tl": 204.380000, "Pb": 207.200000, "Bi": 208.980400, "Po": 209.000000, "At": 210.000000, "Rn": 222.000000, "Fr": 223.000000, "Ra": 226.000000, "Ac": 227.000000, "Th": 232.037700, "Pa": 231.035880, "U": 238.028910, "Np": 237.000000, "Pu": 244.000000, "Am": 243.000000, "Cm": 247.000000, "Bk": 247.000000, "Cf": 251.000000, "Es": 252.000000, "Fm": 257.000000, "Md": 258.000000, "No": 259.000000, "Lr": 262.000000, "Rf": 267.000000, "Db": 268.000000, "Sg": 271.000000, "Bh": 274.000000, "Hs": 269.000000, "Mt": 276.000000, "Ds": 281.000000, "Rg": 281.000000, "Cn": 285.000000, "Nh": 286.000000, "Fl": 289.000000, "Mc": 288.000000, "Lv": 293.000000, "Ts": 294.000000}

    total_mass = 0
    for atom in atoms:
        atom_mass = atomic_masses[atom.atom_type]
        total_mass += atom_mass
        for d in range(len(atom.position)):
            cm[d] += atom_mass*atom.position[d]

    for d in range(len(cm)):
        cm[d] /= total_mass

    return cm


def quat_to_rot(q):
    A = [[0, 0, 0] for x in range(3)]
    s = 1.0/(q[0]**2 + q[1]**2 + q[2]**2 + q[3]**2)
    A[0][0] = 1 - 2*s*(q[2]**2 + q[3]**2)
    A[0][1] = 2*s*(q[1]*q[2] - q[3]*q[0])
    A[0][2] = 2*s*(q[1]*q[3] + q[2]*q[0])
    A[1][0] = 2*s*(q[1]*q[2] + q[3]*q[0])
    A[1][1] = 1 - 2*s*(q[1]**2 + q[3]**2)
    A[1][2] = 2*s*(q[2]*q[3] - q[1]*q[0])
    A[2][0] = 2*s*(q[1]*q[3] - q[2]*q[0])
    A[2][1] = 2*s*(q[2]*q[3] + q[1]*q[0])
    A[2][2] = 1 - 2*s*(q[1]**2 + q[2]**2)

    return A


def rot_to_quat(A):
    quat = []
    t = A[0][0] + A[1][1] + A[2][2]
    r = np.sqrt(1 + t)
    quat.append(r / 2)
    x = np.copysign(
        0.5 * np.sqrt(1 + A[0][0] - A[1][1] - A[2][2]), A[2][1] - A[1][2])
    y = np.copysign(
        0.5 * np.sqrt(1 - A[0][0] + A[1][1] - A[2][2]), A[0][2] - A[2][0])
    z = np.copysign(
        0.5 * np.sqrt(1 - A[0][0] - A[1][1] + A[2][2]), A[1][0] - A[0][1])

    quat.append(x)
    quat.append(y)
    quat.append(z)

    return quat


def quat_pdb(target_mol2, wfn):
    target_mols, _ = read_mol2file(target_mol2)
    wfn_mols, _ = read_wfnfile(wfn)

    for target_mol, wfn_mol in zip(target_mols, wfn_mols):
        assert(len(target_mol) == len(wfn_mol))

        target_gm = center_of_geom(target_mol)
        wfn_gm = center_of_geom(wfn_mol)

        pos1 = []
        pos2 = []

        for i in range(len(target_mol)):
            pos1.append(
                (np.array(target_mol[i].position) - np.array(target_gm))/0.529177)
            pos2.append(np.array(wfn_mol[i].position) - np.array(wfn_gm))

        U = kabsch(pos2, pos1)

        quat = rot_to_quat(U)
        for qi in range(1, 3):
            quat[qi] = -quat[qi]

        trans = -(np.array(target_gm)/0.529177 + np.array(wfn_gm))

        return quat, trans


def rmsd(mol1, mol2, aligned=False):
    rmsd = 0.0
    rmsd_pairs = []
    for a1, atom1 in enumerate(mol1):
        min_dist = 1.0e+100
        min_dist_pair = []
        for a2, atom2 in enumerate(mol2):
            if atom1.element_type == atom2.element_type and len(atom1.neighbors) == len(atom2.neighbors):
                dist = atom1.distance_sq(atom2.position)

                if min_dist > dist:
                    min_dist = dist
                    min_dist_pair = [a1, a2]

        rmsd += min_dist
        rmsd_pairs.append(min_dist_pair)

    rmsd = np.sqrt(rmsd / len(mol1))
    return rmsd, rmsd_pairs


def translate(atoms, disp):
    for atom in atoms:
        for di, d in enumerate(disp):
            atom.position[di] += d

    return atoms


def rotate(atoms, rot):

    # If "rot" parameter is quaternions, convert it to rotation matrix
    if len(rot) == 4:
        rot = quat_to_rot(rot)

    rot_atoms = []
    for atom in atoms:
        p = [[x] for x in atom.position]
        pp = np.matmul(rot, p)

        atom.position = [x[0] for x in pp]
        rot_atoms.append(atom)

    return rot_atoms


def rotate_matrix(atoms, rot):

    for ai, _ in enumerate(atoms):
        atoms[ai].position = np.dot(atoms[ai].position, rot)

    return atoms


def atom_cnt(mol):
    atom_cnt_ = {}
    for atom in mol:
        if atom.element_type in atom_cnt_.keys():
            atom_cnt_[atom.element_type] += 1
        else:
            atom_cnt_[atom.element_type] = 0

    return atom_cnt_


def have_same_constituents(mol1, mol2):
    atom_cnt1 = atom_cnt(mol1)
    atom_cnt2 = atom_cnt(mol2)

    for pair in sorted(atom_cnt1.items()):
        print(pair[0], ':', pair[1], end=', ')

    print("")
    for pair in sorted(atom_cnt2.items()):
        print(pair[0], ':', pair[1], end=', ')

    return atom_cnt1 == atom_cnt2


def molecular_weight(mol):
    atomic_masses = {"H": 1.008000, "He": 4.002602, "Li": 6.940000, "Be": 9.012183, "B": 10.810000, "C": 12.011000, "N": 14.007000, "O": 15.999000, "F": 18.998403, "Ne": 20.179700, "Na": 22.989769, "Mg": 24.305000, "Al": 26.981538, "Si": 28.085000, "P": 30.973762, "S": 32.060000, "Cl": 35.450000, "Ar": 39.948000, "K": 39.098300, "Ca": 40.078000, "Sc": 44.955908, "Ti": 47.867000, "V": 50.941500, "Cr": 51.996100, "Mn": 54.938044, "Fe": 55.845000, "Co": 58.933194, "Ni": 58.693400, "Cu": 63.546000, "Zn": 65.380000, "Ga": 69.723000, "Ge": 72.630000, "As": 74.921595, "Se": 78.971000, "Br": 79.904000, "Kr": 83.798000, "Rb": 85.467800, "Sr": 87.620000, "Y": 88.905840, "Zr": 91.224000, "Nb": 92.906370, "Mo": 95.950000, "Tc": 97.907210, "Ru": 101.070000, "Rh": 102.905500, "Pd": 106.420000, "Ag": 107.868200, "Cd": 112.414000, "In": 114.818000, "Sn": 118.710000, "Sb": 121.760000, "Te": 127.600000, "I": 126.904470, "Xe": 131.293000, "Cs": 132.905452, "Ba": 137.327000, "La": 138.905470, "Ce": 140.116000, "Pr": 140.907660, "Nd": 144.242000,
                     "Pm": 144.912760, "Sm": 150.360000, "Eu": 151.964000, "Gd": 157.250000, "Tb": 158.925350, "Dy": 162.500000, "Ho": 164.930330, "Er": 167.259000, "Tm": 168.934220, "Yb": 173.045000, "Lu": 174.966800, "Hf": 178.490000, "Ta": 180.947880, "W": 183.840000, "Re": 186.207000, "Os": 190.230000, "Ir": 192.217000, "Pt": 195.084000, "Au": 196.966569, "Hg": 200.592000, "Tl": 204.380000, "Pb": 207.200000, "Bi": 208.980400, "Po": 209.000000, "At": 210.000000, "Rn": 222.000000, "Fr": 223.000000, "Ra": 226.000000, "Ac": 227.000000, "Th": 232.037700, "Pa": 231.035880, "U": 238.028910, "Np": 237.000000, "Pu": 244.000000, "Am": 243.000000, "Cm": 247.000000, "Bk": 247.000000, "Cf": 251.000000, "Es": 252.000000, "Fm": 257.000000, "Md": 258.000000, "No": 259.000000, "Lr": 262.000000, "Rf": 267.000000, "Db": 268.000000, "Sg": 271.000000, "Bh": 274.000000, "Hs": 269.000000, "Mt": 276.000000, "Ds": 281.000000, "Rg": 281.000000, "Cn": 285.000000, "Nh": 286.000000, "Fl": 289.000000, "Mc": 288.000000, "Lv": 293.000000, "Ts": 294.000000}
    mw = 0.0
    for atom in mol:
        mw += atomic_masses[atom.element_type]

    return mw


def chemical_formula(mol):
    formula = ''
    atom_cnt = {'C': 0, 'H': 0, 'N': 0, 'O': 0, 'S': 0}

    for atom in mol:
        if atom.element_type in atom_cnt.keys():
            atom_cnt[atom.element_type] += 1
        else:
            atom_cnt[atom.element_type] = 1

    for atom_type in atom_cnt.keys():
        if (atom_cnt[atom_type] > 0):
            formula += "%s%d" % (atom_type, atom_cnt[atom_type])

    return formula
