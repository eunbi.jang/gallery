import os
import subprocess
import pymysql.cursors
from convert_solvation_atomtypes import convert_solvation_atomtypes

# connection 정보
conn = pymysql.connect(
    read_default_file="~/.my.cnf",
    db='quest',  # db name
    charset='utf8',
    autocommit=False)

curs = conn.cursor()

for compound_id in range(2759848,2779847):

    mol2 = "select mol2 from merge where compound_id = '%s' ;" %(compound_id)
    curs.execute(mol2)
    mol2str = curs.fetchall()

    converted_mol = convert_solvation_atomtypes(mol2str[0][0])
    mol2_dir = ('/home/eunbijang/solvation_compound/2759847_converted_mol2/%s.mol2') % compound_id

    with open(mol2_dir, 'w') as fw:
        fw.write(converted_mol)

    file_name = str(compound_id) + '.mol2'
    print("*******")
    print(file_name)
    print("*******")
    cmd = './prepare_solvation_features -m %s ' % (
            "/home/eunbijang/solvation_compound/2759847_converted_mol2/" + file_name)
    print("------")
    print(cmd)
    print("------")
    solv = subprocess.check_output(cmd.split()).decode('utf-8')
    json_dir = ('/home/eunbijang/solvation_compound/2759847_json/%s.json') % compound_id
    f = open(json_dir,'a')
    sql = "update merge set solv = '%s' where Compound_id = %s; " % (solv, compound_id)
    curs.execute(sql)
        
    f.write(solv)
    f.close()
   
  
#conn.commit()
conn.close()
