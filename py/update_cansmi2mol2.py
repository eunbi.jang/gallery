'''
2021.04.10
Eunbi Jang
'''


import os
import pymysql.cursors
import subprocess

conn = pymysql.connect(
    read_default_file="~/.my.cnf",
    db='quest',  # db name
    charset='utf8',
    autocommit=False
)

curs = conn.cursor()

for mol2_id in range(7217400,7217405):
    cids = 'select compound_id from mol2 where mol2_id = %s ' % mol2_id
    curs.execute(cids)
    cid = curs.fetchall()
    
    cansmis= 'select cansmi from compound where compound_id = %s ' % cid[0][0]
    curs.execute(cansmis)
    cansmi = curs.fetchall()
    
    file_name = str(cid[0][0]) + '.mol2'
    f = open('/home/eunbijang/cansmi/' + file_name, 'a') 
    f.write(cansmi[0][0])
    f.close()

    cmd = "obabel -i can -o mol2 %s " % ('/home/eunbijang/cansmi/' + file_name)
    mol2 = subprocess.check_output(cmd.split()).decode('utf-8')

    sql = 'update mol2 set mol2 = "%s" where compound_id = %s ' % (mol2, cid[0][0])
    print(cid[0][0])
    curs.execute(sql)


conn.commit()
conn.close()