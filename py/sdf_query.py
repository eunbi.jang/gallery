"""
sdf에서 db에 넣을 정보 쿼리문으로 가져오는 코드 
"""

#!/usr/bin/env python3
import sys
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('filename', type=str,
                    default='Enamine_advanced_collection_202007.sdf')
parser.add_argument('-o', '--output', type=str, required=True, default='1-1.txt')

args = parser.parse_args()

f = open(args.filename)
fo = open(args.output, 'a')

sdf = ''

def info(sdf):
    split = sdf.split()

    properties = ['<idnumber>', '<heavy_atoms>', '<LogS>', '<LogP>',
                  '<rotating_bonds>', '<PSA>', '<hb_acceptors>', '<hb_donors>', '<SaltType>']

    property_map = {}

    for idx, word in enumerate(split):
        if word in properties:
            property_map[word] = split[idx+2]

    if '<SaltType>' in property_map.keys():
        property_map['<SaltType>'] = "'%s'" % property_map['<SaltType>']

    for property in properties:
        if not property in property_map.keys():
            property_map[property] = 'NULL'

    zid_query = "insert into ZID(ZID) values ('%s')" % (
        property_map['<idnumber>'])
    compound_query = "insert into Compound(heavy_atoms, LogS, LogP, Rotating_bounds, PSA, HB_acceptors, HB_donors, Salt_type) values (%s, %s, %s, %s, %s, %s, %s, %s)" % (
        property_map['<heavy_atoms>'], property_map['<LogS>'], property_map['<LogP>'], property_map['<rotating_bonds>'], property_map['<PSA>'], property_map['<hb_acceptors>'], property_map['<hb_donors>'], property_map['<SaltType>'])

    print(zid_query, file=fo)
    print(compound_query, file=fo)


for line in f:
    sdf = sdf + line
    if '$$$$' in line:
        sd = sdf.split('M  END')[1]
        # print sd
        info(sd)
        sdf = ''

fo.close()
