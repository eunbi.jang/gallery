"""Draws 2D image in SVG with rdkit and return the text.


Usage: python3 -m draw2dimg 'Cl.C[C@@H](F)CN'

Output: SVG Text.

Authored by Hongsuk Kang
Created: 03-28-2021

Copyright (c) 2021 Quantum Intelligence Corporation
"""
import sys
import argparse
from rdkit import Chem
from rdkit.Chem import AllChem
from rdkit.Chem import Draw
import pathlib


def draw_svg_with_smiles(smiles, imgsize=(400, 400)):
    """Draws 2d image of a molecule with smiles and return SVG text.

    Args:
        smiles (str): A SMILES code.
        imgsize (tuple, optional): The width and height of the image. Defaults to (400, 400).

    Returns:
        str: A SVG text.
    """

    mol = Chem.MolFromSmiles(smiles)

    AllChem.Compute2DCoords(mol)

    drawer = Draw.rdMolDraw2D.MolDraw2DSVG(400, 400)
    drawer.DrawMolecule(mol)
    drawer.FinishDrawing()
    svg = drawer.GetDrawingText()

    return svg


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("smiles", nargs=1, type=str)

    args = parser.parse_args()

    svg = draw_svg_with_smiles(args.smiles[0])

    print (svg, file=sys.stdout)
