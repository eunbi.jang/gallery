'''
2021.08.24
Eunbi Jang
'''
import os
import pymysql.cursors
import subprocess

conn = pymysql.connect(
    read_default_file="~/.my.cnf",
    db='quest',  # db name
    charset='utf8',
    autocommit=True
)

curs = conn.cursor()

DATA_DIR = "/home/eunbijang/210624_mol2/"
FILE_PATH = os.listdir(DATA_DIR)

for idx, file_name in enumerate(FILE_PATH):
   
    enamine_id = file_name.split(".")[0]
    print(idx)
    print(enamine_id)

    cids = "select compound_id from enamine_id where enamine_id = '%s';" % enamine_id
    curs.execute(cids)
    cid = curs.fetchall()

    if cid:
        try:
            compound_id = cid[0][0]
            print(compound_id)

            with open(DATA_DIR+file_name, "r") as f:
                mol2s = f.readlines()
                mol2 = ''.join(mol2s)
                
                #print(mol2)
                sql = "update mol2 set mol2 = '%s' where compound_id = %s;" % (mol2, compound_id)
                curs.execute(sql)
        except Exception as ex:
            print(ex)

#conn.commit()
conn.close()