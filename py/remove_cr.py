"""
    Eunbi Jang
    2021.09.08
    remove carriage return from chemdiv_id
"""

import argparse
import os
import subprocess
import pymysql.cursors


idx = 0

path_dir = '/home/eunbijang/zinc'
file_list = os.listdir(path_dir)
# connection 정보
conn = pymysql.connect(
    read_default_file="~/.my.cnf",
    db='quest',  # db name
    charset='utf8',
    autocommit=True)

curs = conn.cursor()

for i in range(8020, 3978985): #3978985, 7813785
    select_id = "select chemspace_id from chemspace where compound_id = %s; " % i
    curs.execute(select_id)
    chemdiv_id = curs.fetchall()

    if chemdiv_id:
        ids = chemdiv_id[0][0].replace("\r", "")
        sql = "update chemspace set chemspace_id = '%s' where compound_id = %s; " % (ids, i)
        curs.execute(sql)
    else:
        print(i)


conn.close()