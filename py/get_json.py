import os
import subprocess
import pymysql.cursors
import argparse



# connection 정보
conn = pymysql.connect(
    read_default_file="~/.my.cnf",
    db='quest',  # db name
    charset='utf8',
    autocommit=False)

curs = conn.cursor()


for compound_id in range(2675047, 2759847):
    #print(compound_id)
    mol2 = "select solv from Merge where compound_id = '%s' ;" % (compound_id)
    curs.execute(mol2)
    solv_str = curs.fetchall()
    try:
        solv = solv_str[0][0]
        mol2_dir = ('/home/eunbijang/solvation_compound/zinc_w_nosolv/%s.json') % compound_id

        with open(mol2_dir, 'w') as fw:
            fw.write(solv)
            file_name = str(compound_id) + '.json'
    except:
        print("null")

