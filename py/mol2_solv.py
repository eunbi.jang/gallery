import os
import subprocess
import pymysql.cursors
from convert_solvation_atomtypes import convert_solvation_atomtypes

path_dir = '/home/eunbijang/chembl' #cheml or zinc file directory
file_list = os.listdir(path_dir)
idx = 0

# connection 정보
conn = pymysql.connect(
    read_default_file="~/.my.cnf",
    db='quest',  # db name
    charset='utf8',
    autocommit=False)

curs = conn.cursor()


for i,list in enumerate(file_list):
    '''
    if i <79040:
        continue
    '''
    #path  = os.path.join(path_dir, list)
     
    #fo = open(path)
    #lines = fo.readlines()
    unique_id = list.split('.')[0]
    print(unique_id)

    try:
    
        #MySQL 은 Oracle 과는 달리 UPDATE 나 DELETE 시 자기 테이블의 데이타를 바로 사용 못하므로 에러발생
        #update Merge set solv = 'a' where compound_id =
        #( select u.compound_id from Merge as m join UniqueID as u on u.compound_id = m.compound_id where u.unique_id = 'CHEMBL99');
        
        comp_id = "select compound_id from UniqueID where unique_id = '%s' ;" %(unique_id)
        
        curs.execute(comp_id)
        cid = curs.fetchall()
        id = ''
        for id in cid:
            id = id[0]
        print(id)
        mol2 = "select mol2 from Merge where compound_id = '%s' ;" %(id)
        curs.execute(mol2)
        mol2str = curs.fetchall()

        converted_mol = convert_solvation_atomtypes(mol2str[0][0])
        mol2_dir = ('/home/eunbijang/solvation_compound/converted_mol2/%s.mol2') % unique_id

        with open(mol2_dir, 'w') as fw:
            fw.write(converted_mol)

        cmd = './prepare_solvation_features -m %s ' % ("/home/eunbijang/solvation_compound/converted_mol2/" + list) 
        solv = subprocess.check_output(cmd.split()).decode('utf-8')


        json_dir = ('/home/eunbijang/solvation_compound/json2_chembl/%s.json') % id
        f = open(json_dir,'a')
        sql = "update Merge set solv = '%s' where Compound_id = %s; " % (solv,id)

        curs.execute(sql)
        
        f.write(solv)
        f.close()
    except:
        print('error')

    

    print(i)


#conn.commit()
conn.close()
