import os
import subprocess
import pymysql.cursors
from convert_solvation_atomtypes import convert_solvation_atomtypes
import argparse



parser = argparse.ArgumentParser()
parser.add_argument('--list', '-l', type=str, required=True)

args = parser.parse_args()

idx = 0

file_list = []
with open(args.list, 'r') as f:
    file_list = f.readlines()

# connection 정보
conn = pymysql.connect(
    read_default_file="~/.my.cnf",
    db='quest',  # db name
    charset='utf8',
    autocommit=False)

curs = conn.cursor()


for i, list in enumerate(file_list):

    compound_id = list.split('.')[0]


    try:
        mol2 = "select mol2 from Merge where compound_id = '%s' ;" % (compound_id)
        curs.execute(mol2)
        mol2str = curs.fetchall()

        converted_mol = convert_solvation_atomtypes(mol2str[0][0])
        mol2_dir = ('/home/eunbijang/solvation_compound/null_converted_mol2/%s.mol2') % compound_id

        with open(mol2_dir, 'w') as fw:
            fw.write(converted_mol)
        file_name = str(compound_id) + '.mol2'

        cmd = './prepare_solvation_features -m %s ' % ("/home/eunbijang/solvation_compound/null_converted_mol2/" + file_name)

        solv = subprocess.check_output(cmd.split()).decode('utf-8')

        json_dir = ('/home/eunbijang/solvation_compound/null_json/%s.json') % compound_id

        f = open(json_dir, 'a')
        f.write(solv)
        f.close()

        sql = "update Merge set solv = '%s' where Compound_id = %s; " % (solv, compound_id)

        curs.execute(sql)

    except:
        print('error')

conn.close()
