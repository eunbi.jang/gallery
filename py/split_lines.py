#!/usr/bin/env python3

import sys
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--lines", "-n", type=int, default=20)
parser.add_argument("filename")

args = parser.parse_args()

with open(args.filename, 'r') as f:
    lines = f.readlines()
    lines_per_file = int(len(lines) / args.lines) + 1

    global_li = 0
    for fi in range(args.lines):
        with open("x%02d" % fi, 'w') as fw:
            for local_li in range(lines_per_file):
                if global_li < len(lines):
                    fw.write(lines[global_li])
                    global_li += 1
