"""
    Eunbi Jang
    2021.09.24
    check duplicated data if chempsace_id is existing
"""

import argparse
import os
import subprocess
import pymysql.cursors



# connection 정보
conn = pymysql.connect(
    read_default_file="~/.my.cnf",
    db='quest',  # db name
    charset='utf8',
    autocommit=True)

curs = conn.cursor()

for i in range(7616412, 7813786): 
    select_id = "select cansmi from compound where compound_id = %s; " % i
    curs.execute(select_id)
    cansmi = curs.fetchall()

    if cansmi:
        sql = "select compound_id from compound where cansmi = '%s'; " % (cansmi[0][0])
        curs.execute(sql)
        cid = curs.fetchall()
        if len(cid[0]) > 1:
            print(cid[0][0])
    else:
        print(i)


conn.close()