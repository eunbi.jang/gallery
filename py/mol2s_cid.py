"""
2021.03.03
Eunbi Jang 
"""

#insert original_mol2s and mol2s to unique_id table and generate compound_id

import os
import pymysql.cursors

conn = pymysql.connect(
    host='192.168.10.2',  # host name
    port=3828,
    user='eunbi',  # user name
    password='Toor#!00',  # password
    db='quest',  # db name
    charset='utf8',
    autocommit=True
)

curs = conn.cursor()

#original_mol2s => 44668
path_dir = '/home/eunbijang/solvation_compound/original_mol2s'
file_list = os.listdir(path_dir)
print(len(file_list))

compound_id = 2779847

for list in file_list:
    #sql = "insert into merge() values();"
    #curs.execute(sql)
    #sql = "insert into unique_id(compound_id, unique_id) values(%s, '%s');" % (compound_id, list.split('.')[0])
    sql= "update unique_id set unique_id ='%s' where compound_id = %s; "% (list.split('.')[0], compound_id)
    curs.execute(sql)
    compound_id += 1

conn.close()
