"""
convert mol2 to smiles for error smiles

Usage: python3 mol2tosmiles.py

Output:

Authored by Eunbi Jang
Created: 12-Jan-2022

Copyright (c) 2021 Quantum Intelligence Corporation
"""
from rdkit import Chem
from rdkit.Chem import AllChem
import pymysql.cursors
import pymysql
import os
class Database():
    def __init__(self):
        self.conn = pymysql.connect(read_default_file="~/.my.cnf",
                                    db='quest',  # db name
                                    charset='utf8',
                                    autocommit=False)
        self.curs = self.conn.cursor()
    
    def __enter__(self):
        return self
    
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()
    
    def execute(self, query, params=None):
        self.curs.execute(query, params or())

    def fetchall(self):
        return self.curs.fetchall()
    
    def commit(self):
        self.conn.commit()

    def close(self, commit=True):
        if commit:
            self.commit()
        self.conn.close()
    

with Database() as db:
    with open("error.txt", "r") as fw:
        error_num = fw.readlines()
      
        with open("rdkit_smiles.smi", "w") as fs:# get .smi file of converted smiles 
            for idx, num in enumerate(error_num):
                sql = "select mol2 from mol2 where compound_id = %s;" % (int(num)+2654633)
                db.execute(sql)
                mol2 = db.fetchall()
                with open("error_mol2/" + f'{num.strip()}.mol2', "w") as f:
                    f.write(mol2[0][0])
                    
                mol2_mol = Chem.MolFromMol2File("error_mol2/" + f'{num.strip()}.mol2') # mol2 to mol
                try:
                    smiles = Chem.MolToSmiles(mol2_mol) # mol to smiles
                except Exception as e:
                    print(num)
                else:
                    fs.write(smiles) 
                    fs.write("\n")






              