"""
2022.01.14
Eunbi Jang

convert smiles to mol2vec vector
"""

from gensim.models import word2vec
import pickle
from mol2vec import features
from mol2vec import helpers
from rdkit import Chem
from mol2vec.features import mol2alt_sentence
import pandas as pd

model = word2vec.Word2Vec.load('model.pkl')

# sentence = features.mol2alt_sentence(Chem.MolFromSmiles("CN1C(=NC(=O)C(=O)N1)SCC2=C(N3C(C(C3=O)NC(=O)C(=NOC)C4=CSC(=N4)N)SC2)C(=O)O"), 1)

smi_dict = {
    "A01" : "CCN",
    "A02" : "CCCN",
    "A03" : "CC(C)CN",
    "A04" : "CCCCOC(C)C",
    "A05" : "C1COCCN1CCCN",
    "A06" : "C1=CC(=CC=C1CN)Cl",
    "A07" : "C1=CC=C(C=C1)CCCCN",
    "A08" : "C1OC2=C(O1)C=C(C=C2)CN",
    "A09" : "CC(C)CC1=CC(=NO1)CN",
    "A10" : "C1=CC=C(C(=C1)CN)CN2C=CC=N2",
    "A11" : "COCCN",
    "A12" : "CCCCCN",
    "A13" : "C1=CC=C(C=C1)CN",
    "A14" : "C1CCC(CC1)CN",
    "A15" : "C1COCCC1CN",
    "A16" : "C1=CC(=CN=C1)CCN",
    "A17" : "C1=CC=C(C=C1)CCCN",
    "A18" : "C1=CC=C(C=C1)OCCN",
    "A19" : "CN1C=C(C=N1)CCCN",
    "A20" : "C1CC(=O)N(C1)CCCN",
    "D-Abu" : "CCC(C(=O)O)N", 
    "D-Nva" : "CCCC(C(=O)O)N", 
    "D-Leu" : "CC(C)CC(C(=O)O)N", 
    "D-phe" : "C1=CC=C(C=C1)CC(C(=O)O)N", 
    "D-FPhe" : "C1=CC(=CC=C1CC(C(=O)O)N)F",
    "D-Cha" : "C1=CC=C(C(=C1)CC(C(=O)O)N)F", 
    "D-F2Phe" : "C1=CC(=C(C=C1CC(C(=O)O)N)F)F",
    "D-1-Nal" : "C1=CC=C2C(=C1)C=CC=C2CC(C(=O)O)N",
    "L-Abu" : "CCC(C(=O)O)N", 
    "L-Nva" : "CCCC(C(=O)O)N", 
    "L-Leu" : "CC(C)CC(C(=O)O)N",
    "L-phe" : "C1=CC=C(C=C1)CC(C(=O)O)N",
    "L-FPhe" : "C1=CC(=CC=C1CC(C(=O)O)N)F", 
    "L-hphe" : "C1=CC=C(C=C1)CCC(C(=O)O)N", 
    "L-MeOPhe": "COC1=CC=CC(=C1)CC(C(=O)O)N",
    "L-NPhe" : "C1=CC(=CC=C1CC(C(=O)O)N)[N+](=O)[O-]",
    "L-Nva" : "CCCC(C(=O)O)N",
    "D-(NMe)Leu": "CC(C)CC(C(=O)O)NC", 
    "L-(NMe)Ala":"CC(C(=O)O)NC",
    "D-(NMe)Ala" : "CC(C(=O)O)NC", 
    "L-beta-homoPhe" : "C1=CC=C(C=C1)CC(CC(=O)O)N",
    "g" : "C(C(=O)O)N",
    "a" : "C[C@@H](C(=O)O)N",
    "v " : "CC(C)[C@@H](C(=O)O)N",
    "l" : "CC(C)C[C@@H](C(=O)O)N",
    "i" : "CC[C@H](C)[C@@H](C(=O)O)N",
    "m" : "CSCC[C@@H](C(=O)O)N",
    "p" : "C1C[C@H](NC1)C(=O)O",
    "f" : "C1=CC=C(C=C1)C[C@@H](C(=O)O)N",
    "y" : "C1=CC(=CC=C1C[C@@H](C(=O)O)N)O",
    "w" : "C1=CC=C2C(=C1)C(=CN2)C[C@@H](C(=O)O)N",
    "s" : "C([C@@H](C(=O)O)N)O",
    "t" : "C[C@H]([C@@H](C(=O)O)N)O",
    "n" : "C([C@@H](C(=O)O)N)C(=O)N",
    "c" : "C([C@@H](C(=O)O)N)S",
    "q" : "C(CC(=O)N)[C@@H](C(=O)O)N",
    "k" : "C(CCN)C[C@@H](C(=O)O)N",
    "r" : "C(C[C@@H](C(=O)O)N)CN=C(N)N",
    "h" : "C1=C(NC=N1)C[C@@H](C(=O)O)N",
    "d" : "C([C@@H](C(=O)O)N)C(=O)O",
    "e" : "C(CC(=O)O)[C@@H](C(=O)O)N"

}

name = [] # amino acid name
smi_list = [] # smiles
vec = [] #vector

for idx, smi in enumerate(smi_dict):
    #print(smi_dict[smi]) # value만 가져오기
    name.append(smi)
    sentence = features.mol2alt_sentence(Chem.MolFromSmiles(smi_dict[smi]), 1)
    vec.append(sentence)
    smi_list.append(smi_dict[smi])
    #print(vec)


df = pd.DataFrame(name)
df["smi"] = smi_list
df["vec"] = vec

df.to_csv("amino_acid_vector.csv")

