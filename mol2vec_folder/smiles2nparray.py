from gensim.models import word2vec
import pickle
from mol2vec import features
from mol2vec import helpers
from rdkit import Chem
from mol2vec.features import mol2alt_sentence, mol2sentence, MolSentence, DfVec, sentences2vec
import json
import numpy as np



model = word2vec.Word2Vec.load('/home/eunbijang/gallery/mol2vec_folder/model.pkl')


regularized_words = []

with open("smiles.txt", "r") as f:
    smiles = f.readlines()
    for idx,smi in enumerate(smiles):
        print(idx)
        sentence = mol2alt_sentence(Chem.MolFromSmiles(smi), 1) # smiles to word list
        smiles_vector = [] # 다음 smiles 로 넘어갈때 여기서 초기화 해줘야함 !!!!!
        for word in sentence: # word in word list
            vector = model.wv[word] # convert word to vector
            smiles_vector.append(vector) # vector list for words in word list 
            
        regularized_words.append(smiles_vector)# vector for each smiles
                
regularized_words = np.array(regularized_words)
print(type(regularized_words))