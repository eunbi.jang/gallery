"""
update sequence of pampa

Usage: python3 get_sequence.py

Output:

Authored by Eunbi Jang
Created: 10-26-2021

Copyright (c) 2021 Quantum Intelligence Corporation
"""

import pandas as pd
import pymysql.cursors

conn = pymysql.connect(
    read_default_file="~/.my.cnf",
    db='quest',  # db name
    charset='utf8',
    autocommit=False
)

curs = conn.cursor()

pd.set_option('display.max.colwidth', 500)

FILE_PATH = "/home/eunbijang/gallery/curated-hexamer-permeability-data.xlsx"
#tot_len = len(pd.read_excel(FILE_PATH, usecols = [0], header = None))
df = pd.DataFrame(pd.read_excel(FILE_PATH))

#smiles = pd.read_excel(FILE_PATH, usecols = [10], header = None)
seq_df = df.iloc[:,[10,21,23,24,26,27,29,30,32,33,35]]
#print(str(smiles.loc[1]).split("\n")[0])  #Unnamed: 21        ?

def get_seq(seq_list):
    ans = ""
    for idx, val in enumerate(seq_list):
        if idx %2 == 1:# dl idx is odd in this list
            if val == "D":# lower case
                seq_list[idx+1] = seq_list[idx+1].lower()
            elif val == "L": # upper case
                seq_list[idx+1] = seq_list[idx+1].upper()
            
            if len(seq_list[idx+1]) > 1:
                ans += "(%s)" % seq_list[idx+1]
            else:
                ans += seq_list[idx+1]
    return ans

def update_db(i, smiles,seq):
    try:
        sql = "select compound_id from pampa_peptide where smiles = '%s'; " % (smiles)
        curs.execute(sql)
        cid = curs.fetchall()
        print(cid[0][0], i)
        update_seq = "update pampa_peptide set sequence = '%s' where compound_id = '%s'; " % (ans, cid[0][0])
        print(ans)
        #curs.execute(update_seq)
    except Exception as e:
        print(i)
        print(e)
    return True

for idx in range(1, len(seq_df)):
    print(idx)
    seq_list = str(seq_df.loc[idx]).split("\n")
    
    for idx,val in enumerate(seq_list):
        if idx == 0:#smiles
            seq_list[idx] = val.split()[1]
        else:
            seq_list[idx] = val.split()[2]
    seq_list.pop()
    print(seq_list)
    ans = get_seq(seq_list)
    update_db(idx, seq_list[0], ans)
    break


# def get_id(col_num, idx): #23,26,29,32,35,38
#     id_col = pd.read_excel(FILE_PATH, usecols = [col_num], header = None)
#     ids = str(id_col.loc[idx]).split()[1]
#     return ids

# def get_dl(col_num, idx): #21,24,27,30,33,36
#     dl_col = pd.read_excel(FILE_PATH, usecols = [col_num], header = None)
#     #print(str(dl_line.loc[idx]))
#     dl = str(dl_col.loc[idx]).split()[1]
#     #print(dl)
#     return dl

# dl_list = [21,24,27,30,33,36]
# id_list = [23,26,29,32,35,38]

# def get_seq(dl, ids):
#     ans = ""
#     if dl == "D":# lower case
#         ids = ids.lower()
#     elif dl == "L": # upper case
#         ids = ids.upper()

#     if len(ids) > 1:
#         ans += "(%s)" % ids
#     else:
#         ans += ids

#     return ans

# for idx in range(10, tot_len):  
#     ans = "" 
#     for x, y in zip(dl_list, id_list):
#     #print(x, y)
#         dl = get_dl(x,idx)
#         ids = get_id(y,idx)
#         ans += get_seq(dl,ids)
#     #print(str(smiles.loc[idx]).split()[1])
#     print(ans)
#     try:
#         sql = "select compound_id from pampa_peptide where smiles = '%s'; " % (str(smiles.loc[idx]).split()[1])
#         curs.execute(sql)
#         cid = curs.fetchall()
#         print(cid[0][0])
#         update_seq = "update pampa_peptide set sequence = '%s' where compound_id = '%s'; " % (ans, cid[0][0])
#         curs.execute(update_seq)
#     except Exception as e:
#         print(idx)
#         print(e)


conn.commit()
conn.close()



        
        





