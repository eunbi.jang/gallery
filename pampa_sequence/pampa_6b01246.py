"""
insert id, seq, LogP into pampa_6b01246 table

Usage: python3 pampa_6b01246.py

Output:

Authored by Eunbi Jang
Created: 10-27-2021

Copyright (c) 2021 Quantum Intelligence Corporation
"""

import pandas as pd
import pymysql.cursors

conn = pymysql.connect(
    read_default_file="~/.my.cnf",
    db='quest',  # db name
    charset='utf8',
    autocommit=False
)

curs = conn.cursor()

FILE_PATH = "/home/eunbijang/gallery/pampa_6b01246.xlsx"
df = pd.DataFrame(pd.read_excel(FILE_PATH, header=None, sheet_name = 8))
seq_df = df.iloc[:,[0,1,2,3]]

for idx in range(0,40):
    seq_list = str(seq_df.loc[idx]).split()[1:8:2]  #D, L 구분 제대로 하기 
    
    ans = "(%s)(%s)(mL)(Nle)(pro)(%s)" % (seq_list[1], seq_list[2], seq_list[0]) # R1, R2, R6
    #print(seq_list[3])
    i = format(idx+1, '02')
    if seq_list[3] != "NaN" and seq_list[3] != "Name:" :
        sql = "insert into pampa_6b01246(compound_id, sequence,logP) values('%s', '%s', %s);" % (f"1.10-{i}", ans, seq_list[3])
    else:
        sql = "insert into pampa_6b01246(compound_id, sequence) values('%s', '%s');" % (f"1.10-{i}", ans)

 

    #print(sql)
    curs.execute(sql)

conn.commit()
conn.close()