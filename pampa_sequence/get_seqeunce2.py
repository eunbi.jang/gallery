"""
update sequence of pampa

Usage: python3 get_sequence2.py

Output:

Authored by Eunbi Jang
Created: 10-26-2021

Copyright (c) 2021 Quantum Intelligence Corporation
"""

import pandas as pd
import pymysql.cursors
import subprocess

conn = pymysql.connect(
    read_default_file="~/.my.cnf",
    db='quest',  # db name
    charset='utf8',
    autocommit=False
)

curs = conn.cursor()

pd.set_option('display.max.colwidth', 500)

FILE_PATH = "/home/eunbijang/gallery/ja0c06115_si_004.xlsx"
df = pd.DataFrame(pd.read_excel(FILE_PATH)) #convert to string 
seq_df = df.iloc[:,[9]]
smiles = df.iloc[:,[15]]

def get_seq(seq_list):
    ans = ""
    for idx,val in enumerate(seq_list):
        
        if val == "Acetyl" :
            ans += "(Ace"
            if seq_list[idx+1] == "P":
                ans += "-P)"
            elif seq_list[idx+1] == "p":
                ans += "-p)"
            elif seq_list[idx+1] == "ThrCleaved":
                ans += "-T)"
        
        if idx > 1:
            if val == "ThrCleaved":
                ans += "T"
            else:
                if "***" in val:
                    val = val.replace("***","")
                
                if len(val) >1:
                    if val.isupper():
                        ans += "(%s)" % (val)
                    else:
                        ans += "(%s)" % (val.lower())
                else:
                    ans += "%s" % (val)     
    return ans

def update_db(idx,smiles,seq): #4867부터 
    try:
        sql = "select compound_id from pampa_peptide where smiles = '%s'; " % (smiles)
        curs.execute(sql)
        cid = curs.fetchall()
        #print(cid[0][0])
        update_seq = "update pampa_peptide set sequence = '%s' where compound_id = '%s'; " % (ans, cid[0][0])
        print(idx, ans)
        # with open(str(i)+".smiles", "w") as f:
        #     f.write(smiles)
        # cmd = "obabel -i smi -o mol2 %s " % (str(i)+".smiles")
        # mol2 = subprocess.check_output(cmd.split()).decode('utf-8')
        # i += 4867
        # sql = "insert into pampa_peptide(compound_id,smiles,mol2,sequence) values('%s','%s','%s','%s');" %(f'P{i}',smiles,mol2,seq)
        #curs.execute(sql)
        #curs.execute(update_seq)
        #print(idx)
    except Exception as e:
        print(idx)
        print(e)
    return True
  
for idx in range(1, len(seq_df)):
    row_list = str(seq_df.loc[idx]).split()
    seq_list = row_list[2].split(",")
    # print(seq_list)
    # print("\n")
   
    ans = get_seq(seq_list)
    smi = str(smiles.loc[idx]).split()[1]
    update_db(idx,smi,ans)
    
    #print(seq_list[1].split()[1])


conn.commit()
conn.close()
