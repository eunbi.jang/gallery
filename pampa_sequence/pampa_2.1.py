"""
insert id, seq, LogP into pampa_6b01246 table for each library 
jm6b01246_si_001.pdf

Usage: python3 pampa_2.1.py

Output:

Authored by Eunbi Jang
Created: 10-28-2021

Copyright (c) 2021 Quantum Intelligence Corporation
"""


import pandas as pd
import pymysql.cursors

conn = pymysql.connect(
    read_default_file="~/.my.cnf",
    db='quest',  # db name
    charset='utf8',
    autocommit=False
)

curs = conn.cursor()

"""
Library 2.1
(L_R1)(R2)(mL)(Nle)(Pro)(R6)
           (L)
    
"""

FILE_PATH = "/home/eunbijang/gallery/pampa_2.1.xlsx"
df = pd.DataFrame(pd.read_excel(FILE_PATH, header=None))
seq_df = df.iloc[:,[0,1,2,3,4]] #id, R6, R1, R2, LogP

for idx in range(0, len(seq_df)):
    ans = ""
    seq_list = str(seq_df.loc[idx]).split()[1:10:2] #['0', '2.1-01Me', '1', 'A01', '2', 'Ala', '3', 'A12', 'Name:', '0,', 'dtype:', 'object']
    if "Me" in seq_list[0] : # ex)2.1-01Me
        ans += "(%s)(%s)(mL)(Nle)(Pro)(%s)" % (seq_list[2], seq_list[3], seq_list[1])
        print(ans)
    elif "H" in seq_list[0]:
        ans += "(%s)(%s)(L)(Nle)(Pro)(%s)" % (seq_list[2], seq_list[3], seq_list[1])
        print(ans)
        
    sql = "insert into pampa_6b01246(compound_id, sequence, logP) values('%s', '%s', %s);" % (seq_list[0], ans, seq_list[4])
    curs.execute(sql)

conn.commit()
conn.close()


